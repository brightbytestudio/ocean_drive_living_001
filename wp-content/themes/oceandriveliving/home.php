<?php
/*
Template Name: Home
*/
?>
<?php get_header(); ?>
<div class="full">
		<div class="inner reset innermain" style="background: #fff;">
			<div class="inner" id="hero">
			<ul>
				<li>
					<div class="image_container">
						<img src="<?php echo get_bloginfo('template_directory');?>/images/slider1.jpg">
						<!--<div class="caption">
							<h1>VIRKELYST SOFA, TEAK / GREY</h1>
							<h2>£2,499</h2>
							<br />
							<a class="button" href="<?php echo site_url()?>/product/virkelyst-sofa-teak-grey/">Learn More</a>
						</div>-->
					</div>
				</li>				
				<li>
					<div class="image_container">
						<img src="<?php echo get_bloginfo('template_directory');?>/images/slider2.jpg">
						<!--<div class="caption">
							<h1>OCEAN TABLE, FIBRE CONCRETE</h1>
							<h2>£1,599</h2>
							<br />
							<a class="button" href="<?php echo site_url()?>/product/ocean-table-fibre-concrete/">Learn More</a>
						</div>-->			
					</div>
				</li>
				<li>
					<div class="image_container">
						<img src="<?php echo get_bloginfo('template_directory');?>/images/slider3.jpg">		
					</div>
				</li>	
				<li>
					<div class="image_container">
						<img src="<?php echo get_bloginfo('template_directory');?>/images/slider4.jpg">		
					</div>
				</li>
				<li>
					<div class="image_container">
						<img src="<?php echo get_bloginfo('template_directory');?>/images/slider5.jpg">	
					</div>
				</li>
				<li>
					<div class="image_container">
						<img src="<?php echo get_bloginfo('template_directory');?>/images/slider6.jpg">	
					</div>
				</li>
				<li>
					<div class="image_container">
						<img src="<?php echo get_bloginfo('template_directory');?>/images/slider7.jpg">
					</div>
				</li>
				<li>
					<div class="image_container">
						<img src="<?php echo get_bloginfo('template_directory');?>/images/slider8.jpg">
					</div>
				</li>
				<li>
					<div class="image_container">
						<img src="<?php echo get_bloginfo('template_directory');?>/images/slider9.jpg">
					</div>
				</li>
				<li>
					<div class="image_container">
						<img src="<?php echo get_bloginfo('template_directory');?>/images/slider10.jpg">
					</div>
				</li>		
				<li>
					<div class="image_container">
						<img src="<?php echo get_bloginfo('template_directory');?>/images/slider11.jpg">
					</div>
				</li>																								
			</ul>
			</div>
		<div class="inner feature_boxes border_top border_bottom">
			<div class="half">
				<div class="image_container">
					<a href="<?php echo site_url();?>/product/dania-bottle-opener-teak/"><img src="<?php echo get_bloginfo('template_directory');?>/images/home1.jpeg"></a>
				</div>
				<a href="<?php echo site_url();?>/product/dania-bottle-opener-teak/"><h1>DANIA BOTTLE OPENER, TEAK</h1></a>
				<p>The Dania Bottle Opener is beautifully designed and executed to enhance your dining table – unlike many other bottle openers.
</p>
			</div>
			<div class="half last border_left">
				<div class="image_container">
					<a href="<?php echo site_url();?>/product/dania-onion-box-teak/"><img src="<?php echo get_bloginfo('template_directory');?>/images/home2.jpeg"></a>
				</div>
				<a href="<?php echo site_url();?>/product/dania-onion-box-teak/"><h1>DANIA ONION BOX, TEAK</h1></a>
				<p>The Dania Onion Box can be used like any other box for fruit and vegetables, but it is smaller and far sturdier, radiating the sort of quality that makes an onion box a nostalgic, charming element in the modern home. </p>
			</div>			
		</div>
		<div class="inner picks border_bottom padding_bottom">
		<h1>This month's picks</h1>

<?php
      $my_query = new WP_Query( array(
      'post_status' => 'publish',
      'post_type' => 'product',
      'meta_key' => '_featured',
      'meta_value' => 'yes',
      'posts_per_page' => '4'
      ) );
				


      if ($my_query->have_posts()) : while ($my_query->have_posts()) : $my_query->the_post();
      echo '<div class="quarter"><a class="div_hover" href="';
      the_permalink();
      echo '"></a>';
      echo '<div class="image_container">';
      the_post_thumbnail('medium');
      echo '</div>';
      echo '<h1>';
      the_title();
      echo '</h1></div>';
      endwhile;
      endif;
       
     ?>

			<!--<div class="quarter">
			<a class="div_hover" href="#"></a>
			<div class="image_container">
				<img src="<?php echo get_bloginfo('template_directory');?>/images/picks1.jpg">	
			</div>			
				<h1>DANIA STOOL, OAK</h1>
			</div>

			<div class="quarter">
			<div class="div_hover"></div>
			<div class="image_container">
				<img src="<?php echo get_bloginfo('template_directory');?>/images/picks2.jpg">	
			</div>			
				<h1>DANIA STOOL, OAK</h1>
			</div>
			<div class="quarter">
			<div class="div_hover"></div>
			<div class="image_container">
				<img src="<?php echo get_bloginfo('template_directory');?>/images/picks3.jpg">				
			</div>
				<h1>DANIA STOOL, OAK</h1>
			</div>
			<div class="quarter">
			<div class="div_hover"></div>
			<div class="image_container">
				<img src="<?php echo get_bloginfo('template_directory');?>/images/picks4.jpg">				
			</div>
				<h1>DANIA STOOL, OAK</h1>
			</div>-->									
	</div>
	</div>
		<script type="text/javascript" src="<?php bloginfo('template_url'); ?>/js/unslider.js"></script>

        <script type="text/javascript">
            $(window).load(function() {        
                $('#hero').unslider({
                speed: 1000,               //  The speed to animate each slide (in milliseconds)
                delay: 5000,              //  The delay between slide animations (in milliseconds)
                dots: true,               //  Display dot navigation
                fluid: false              //  Support responsive design. May break non-responsive designs
                });
            });
        </script> 

<?php get_footer(); ?>

