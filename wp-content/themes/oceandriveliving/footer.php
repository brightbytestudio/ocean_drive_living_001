	<div class="full" id="footer">
		<div class="inner">
			<img src="<?php echo get_bloginfo('template_directory');?>/images/oceandriveliving.jpg" id="logo">
			<ul>
				<li><a href="<?php echo site_url();?>">Home</a></li>
				<li><a href="<?php echo site_url();?>/about-us">About Us</a></li>
				<li><a href="<?php echo site_url();?>/delivery-information">Delivery Information</a></li>
				<li><a href="<?php echo site_url();?>/terms-conditions">Terms & Conditions</a></li>
				<li><a href="<?php echo site_url();?>/contact-us">Contact Us</a></li>
				<li><a href="<?php echo site_url();?>/cart">View My Cart</a></li>
			</ul>
			<ul>
				<li>Ocean Drive Living</li>
				<li>71 Parchment Street</li>
				<li>Winchester</li>
				<li>SO23 8AT</li>
				<li style="margin-top: 10px">01962 864 111</li>
				<li style="margin-top: 10px">oceandriveliving@gmail.com</li>
			</ul>
		</div>
		<div class="inner copyright">
		<p class="f-l">
			© 2013 Ocean Drive Living | 01962 864 111 | oceandriveliving@gmail.com
			</p>
			<ul id="social" style="float: right !important; margin-right: 0; margin-top: 10px; font-size: 11px;">
					<li>
						<a href="http://www.instagram.com/oceandriveliving" target="_new"><img src="http://oceandriveliving.co.uk/wp-content/themes/oceandriveliving/images/instagram.jpg">
						Instagram</a>
					</li>
					<li style="margin-right: 0">
						<a href="https://twitter.com/ODLWinchester" target="_new"><img src="http://oceandriveliving.co.uk/wp-content/themes/oceandriveliving/images/twittericon.png">
						Twitter</a>
					</li>
				</ul>
		</div>
	</div>
	<?php wp_footer(); ?>
	<!-- JS -->
	<script src="<?php echo get_bloginfo('template_directory');?>/js/jquery.nivo.slider.pack.js" type="text/javascript"></script>
	<!--<script type="text/javascript">
		$(window).load(function() {
		    $('#hero').nivoSlider({
		        effect: 'fade', // Specify sets like: 'fold,fade,sliceDown'
		        slices: 15, // For slice animations
		        boxCols: 8, // For box animations
		        boxRows: 4, // For box animations
		        animSpeed: 500, // Slide transition speed
		        pauseTime: 3000, // How long each slide will show
		        startSlide: 0, // Set starting Slide (0 index)
		        directionNav: false, // Next & Prev navigation
		        controlNav: true, // 1,2,3... navigation
		        controlNavThumbs: false, // Use thumbnails for Control Nav
		        pauseOnHover: true, // Stop animation while hovering
		        manualAdvance: false, // Force manual transitions
		        prevText: 'Prev', // Prev directionNav text
		        nextText: 'Next', // Next directionNav text
		        randomStart: false, // Start on a random slide
		        beforeChange: function(){}, // Triggers before a slide transition
		        afterChange: function(){}, // Triggers after a slide transition
		        slideshowEnd: function(){}, // Triggers after all slides have been shown
		        lastSlide: function(){}, // Triggers when last slide is shown
		        afterLoad: function(){} // Triggers when slider has loaded
		    });
		});
		</script>-->
			<script>
	$(document).ready(function()
	{
	if($("html").height() > 1200) {

	var $top1= $('html').offset().top + 117;
	$(window).scroll(function()
	{

	    if ($(window).scrollTop()>$top1)
	    {
	     $('.globalnav').addClass('fixed');
	     $('#top').addClass('drop');
	    }
	    else
	    {
	     $('.globalnav').removeClass('fixed');
	     $('#top').removeClass('drop');

	     }
	});
}
	});
	</script>
</body>

</html>
