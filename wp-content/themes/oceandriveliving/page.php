<?php get_header(); ?>
	<?php if (have_posts()) : while (have_posts()) : the_post(); ?>					
		<div class="full" id="page">
		<div class="inner innermain">		
				<?php the_content(); ?>
			</div>
		</div>	
		<?php wp_link_pages(array('before' => 'Pages: ', 'next_or_number' => 'number')); ?>
		<?php endwhile; endif; ?>
<?php get_footer(); ?>
