<?php
/**
 * You are allowed to use this API in your web application.
 *
 * Copyright (C) 2013 by customweb GmbH
 *
 * This program is licenced under the customweb software licence. With the
 * purchase or the installation of the software in your application you
 * accept the licence agreement. The allowed usage is outlined in the
 * customweb software licence which can be found under
 * http://www.customweb.ch/en/software-license-agreement
 *
 * Any modification or distribution is strictly forbidden. The license
 * grants you the installation in one application. For multiuse you will need
 * to purchase further licences at http://www.customweb.com/shop.
 *
 * See the customweb software licence agreement for more details.
 *
*/

$base_dir = dirname(dirname(dirname(dirname(__FILE__))));
require_once $base_dir . '/wp-load.php';
BarclaycardCwUtil::includeClass('BarclaycardCw_Transaction');
BarclaycardCwUtil::includeClass('BarclaycardCw_AdapterFactory');
BarclaycardCwUtil::includeClass('BarclaycardCw_Log');


// TODO: Remove the debug code
$post_dump = print_r($_POST, TRUE);
$get_dump = print_R($_GET, TRUE);
$fp = fopen(dirname(__FILE__).'/request.log', 'a');
fwrite($fp, 'POST:'.$post_dump);
fwrite($fp, 'GET:'.$get_dump."\n\n");
fclose($fp);


$updateAdapter = BarclaycardCw_AdapterFactory::getUpdateAdapter();

$idMap = $updateAdapter->preprocessTransactionUpdate();

$dbTransaction = null;
if ( isset($idMap['transactionId'])) {
	$dbTransaction = BarclaycardCw_Transaction::loadById($idMap['transactionId']);
}
elseif ( isset($idMap['paymentId'])) {
	$dbTransaction = BarclaycardCw_Transaction::getTransactionByPaymentId($idMap['paymentId']);
}

if($dbTransaction == null){
	$message = 'No transaction found with given id. Request: '.http_build_query($_REQUEST);
	BarclaycardCw_Log::add($message);
	$updateAdapter->confirmFailedTransactionUpdate(null, 'No transaction found with the given id.');
}
else {
	try {
		$transaction = $dbTransaction->getTransactionObject();
		$updateAdapter->processTransactionUpdate($transaction, $_REQUEST, BarclaycardCwUtil::getBackendOperationAdapterFactory());
		$dbTransaction->save();
		$updateAdapter->confirmTransactionUpdate($transaction);
	}
	catch (Exception $e) {
		$message = "Updating of transaction with id ".$dbTransaction->getTransactionId()." failed. \nRequest: ".http_build_query($_REQUEST)."\nReason: ".$e->getMessage();
		BarclaycardCw_Log::add($message);
		$updateAdapter->confirmFailedTransactionUpdate($transaction, $e->getMessage());
		
	}
}



