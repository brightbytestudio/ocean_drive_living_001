<?php
/**
  * You are allowed to use this API in your web application.
 *
 * Copyright (C) 2013 by customweb GmbH
 *
 * This program is licenced under the customweb software licence. With the
 * purchase or the installation of the software in your application you
 * accept the licence agreement. The allowed usage is outlined in the
 * customweb software licence which can be found under
 * http://www.customweb.ch/en/software-license-agreement
 *
 * Any modification or distribution is strictly forbidden. The license
 * grants you the installation in one application. For multiuse you will need
 * to purchase further licences at http://www.customweb.com/shop.
 *
 * See the customweb software licence agreement for more details.
 *
 */

require_once 'Customweb/Barclaycard/Method/DefaultMethod.php';
require_once 'Customweb/Barclaycard/Method/OpenInvoice/Intrum.php';
require_once 'Customweb/Barclaycard/Method/OpenInvoice/PostFinanceFis.php';
require_once 'Customweb/Barclaycard/Method/OpenInvoice/AfterPay.php';
require_once 'Customweb/Barclaycard/Method/OpenInvoice/Billpay.php';
require_once 'Customweb/Barclaycard/Method/OpenInvoice/RatePay.php';
require_once 'Customweb/Barclaycard/Method/OpenInvoice/Klarna.php';
require_once 'Customweb/I18n/Translation.php';


class Customweb_Barclaycard_Method_OpenInvoiceMethod extends Customweb_Barclaycard_Method_DefaultMethod {

	public function validate(Customweb_Payment_Authorization_IOrderContext $orderContext, Customweb_Payment_Authorization_IPaymentCustomerContext $paymentContext) {
		parent::validate($orderContext, $paymentContext);
		
		$processor = $this->determineProcessor($orderContext);
		$processor->validate($orderContext, $paymentContext);
	}
	
	public function getFormFields(Customweb_Payment_Authorization_IOrderContext $orderContext, $aliasTransaction, $failedTransaction, $authorizationMethod, $isMoto) {
		$elements = parent::getFormFields($orderContext, $aliasTransaction, $failedTransaction, $authorizationMethod, $isMoto);
	
		$processor = $this->determineProcessor($orderContext);
		$elements = array_merge(
			$elements,
			$processor->getAdditionalFormFields($orderContext, $aliasTransaction, $failedTransaction, $authorizationMethod, $isMoto)
		);
		
		return $elements;
	}
	
	/**
	 * This methdo determines the processor for the invoice.
	 * 
	 * @param Customweb_Payment_Authorization_IOrderContext $orderContext
	 * @return Customweb_Barclaycard_Method_OpenInvoice_IProcessor
	 */
	protected function determineProcessor(Customweb_Payment_Authorization_IOrderContext $orderContext) {
		
		$billingCountry = strtoupper($orderContext->getBillingCountryIsoCode());
		switch($billingCountry) {
			case 'AT':
				return new Customweb_Barclaycard_Method_OpenInvoice_Intrum();		
				
			case 'CH':
				$processorName = strtolower($orderContext->getPaymentMethod()->getPaymentMethodConfigurationValue("processor_ch"));
				switch ($processorName) {
					case 'intrum':
						return new Customweb_Barclaycard_Method_OpenInvoice_Intrum();
					case 'postfinance':
						return new Customweb_Barclaycard_Method_OpenInvoice_PostFinanceFis();
				}
				break;
					
			case 'DE':
				$processorName = strtolower($orderContext->getPaymentMethod()->getPaymentMethodConfigurationValue("processor_de"));
				switch ($processorName) {
					case 'intrum':
						return new Customweb_Barclaycard_Method_OpenInvoice_Intrum();
					case 'afterpay':
						return new Customweb_Barclaycard_Method_OpenInvoice_AfterPay();
					case 'billpay':
						return new Customweb_Barclaycard_Method_OpenInvoice_Billpay();
					case 'klarna':
						return new Customweb_Barclaycard_Method_OpenInvoice_Klarna();
					case 'ratepay':
						return new Customweb_Barclaycard_Method_OpenInvoice_RatePay();
				}
				break;
					
			case 'DK':
				return new Customweb_Barclaycard_Method_OpenInvoice_Klarna();
											
			case 'FI':
				return new Customweb_Barclaycard_Method_OpenInvoice_Klarna();
			
			case 'NL':
				$processorName = strtolower($orderContext->getPaymentMethod()->getPaymentMethodConfigurationValue("processor_de"));
				switch ($processorName) {
					case 'afterpay':
						return new Customweb_Barclaycard_Method_OpenInvoice_AfterPay();
					case 'klarna':
						return new Customweb_Barclaycard_Method_OpenInvoice_Klarna();
				}
				break;
			
			case 'NO':
				return new Customweb_Barclaycard_Method_OpenInvoice_Klarna();
				
			case 'SE':
				return new Customweb_Barclaycard_Method_OpenInvoice_Klarna();
		}
		throw new Exception(Customweb_I18n_Translation::__(
			"The payment method !paymentMethodName is not available - no processor found.",
			array(
				'!paymentMethodName' => $this->getPaymentMethodDisplayName(),
			)
		));
	}
	
	
	public function getPaymentMethodBrandAndMethod(Customweb_Barclaycard_Authorization_Transaction $transaction) {
		$billingCountry = strtoupper($transaction->getTransactionContext()->getOrderContext()->getBillingCountryIsoCode());
		$params = $this->getPaymentMethodParameters();
		return array(
			'pm' => $params['pm'] . ' ' . $billingCountry,
			'brand' => $params['brand'] . ' ' .  $billingCountry,
		);
	}
	
	public function getAuthorizationParameters(Customweb_Barclaycard_Authorization_Transaction $transaction, array $formData, $authorizationMethod) {
		$parameters = parent::getAuthorizationParameters($transaction, $formData, $authorizationMethod);
	
		return array_merge(
			$parameters,
			$this->determineProcessor($transaction->getTransactionContext()->getOrderContext())->getAuthorizationParameters($transaction, $formData, $authorizationMethod)
		);
	}
	
	
}