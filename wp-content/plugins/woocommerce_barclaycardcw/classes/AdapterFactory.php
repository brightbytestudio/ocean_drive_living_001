<?php 
/**
  * You are allowed to use this API in your web application.
 *
 * Copyright (C) 2013 by customweb GmbH
 *
 * This program is licenced under the customweb software licence. With the
 * purchase or the installation of the software in your application you
 * accept the licence agreement. The allowed usage is outlined in the
 * customweb software licence which can be found under
 * http://www.customweb.ch/en/software-license-agreement
 *
 * Any modification or distribution is strictly forbidden. The license
 * grants you the installation in one application. For multiuse you will need
 * to purchase further licences at http://www.customweb.com/shop.
 *
 * See the customweb software licence agreement for more details.
 *
 */

library_load_class_by_name('Customweb_Payment_Authorization_AbstractAdapterFactory');
library_load_class_by_name('Customweb_Payment_Authorization_PaymentPage_IAdapter');
library_load_class_by_name('Customweb_Payment_Authorization_Iframe_IAdapter');
library_load_class_by_name('Customweb_Payment_Authorization_Hidden_IAdapter');
library_load_class_by_name('Customweb_Payment_Authorization_Ajax_IAdapter');
library_load_class_by_name('Customweb_Payment_Authorization_Server_IAdapter');
library_load_class_by_name('Customweb_Payment_Authorization_Recurring_IAdapter');

BarclaycardCwUtil::includeClass('Adapter/AjaxAdapterWrapper');
BarclaycardCwUtil::includeClass('Adapter/HiddenAdapterWrapper');
BarclaycardCwUtil::includeClass('Adapter/IframeAdapterWrapper');
BarclaycardCwUtil::includeClass('Adapter/PaymentPageAdapterWrapper');
BarclaycardCwUtil::includeClass('Adapter/ServerAdapterWrapper');

BarclaycardCwUtil::includeClass('ConfigurationAdapter');


class BarclaycardCw_AdapterFactory extends Customweb_Payment_Authorization_AbstractAdapterFactory{
	
	/**
	 * @var BarclaycardCw_Adapter_IAdapter[]
	 */
	private static $wrapper_instances = array();

	/**
	 * @return BarclaycardCw_Adapter_IAdapter
	 */
	public function getAdapterByOrderContext(Customweb_Payment_Authorization_IOrderContext $context) {
		$adapter = parent::getAdapterByOrderContext($context);
		return $this->wrapAdapter($adapter);
	}
	
	/**
	 *           	   	 		  		
	 * @param string $authorizationMethod
	 * @throws Exception
	 * @return BarclaycardCw_Adapter_IAdapter
	 */
	public function getAdapterByAuthorizationMethod($authorizationMethod) {
		$adapter = parent::getAdapterByAuthorizationMethod($authorizationMethod);
		
		if (!isset(self::$wrapper_instances[$authorizationMethod])) {
			self::$wrapper_instances[$authorizationMethod] = $this->wrapAdapter($adapter);
		}
		
		return self::$wrapper_instances[$authorizationMethod];
	}
	
	private function wrapAdapter($adapter) {
		$class = get_class($adapter);
		$o = new ReflectionClass( $class );
		$authorizationMethod = $o->getConstant("AUTHORIZATION_METHOD_NAME");
		$wrappers = $this->getAdapterWrapperMap();
		if (!isset($wrappers[$authorizationMethod])) {
			return $adapter;
		}
		$wrapperClass = $wrappers[$authorizationMethod];
		return new $wrapperClass($adapter);
	}
	
	protected function getAdapterMap() {
		return array(
			
			Customweb_Payment_Authorization_PaymentPage_IAdapter::AUTHORIZATION_METHOD_NAME => 'Customweb_Barclaycard_Authorization_PaymentPage_Adapter',
			
			
			
			Customweb_Payment_Authorization_Hidden_IAdapter::AUTHORIZATION_METHOD_NAME => 'Customweb_Barclaycard_Authorization_Hidden_Adapter',
			
			
			
			Customweb_Payment_Authorization_Server_IAdapter::AUTHORIZATION_METHOD_NAME => 'Customweb_Barclaycard_Authorization_Server_Adapter',
			
			
			Customweb_Payment_Authorization_Recurring_IAdapter::AUTHORIZATION_METHOD_NAME => 'Customweb_Barclaycard_Authorization_Recurring_Adapter',
			
		);
	}
	
	protected function getAdapterWrapperMap() {
		return array(
			
			Customweb_Payment_Authorization_PaymentPage_IAdapter::AUTHORIZATION_METHOD_NAME => 'BarclaycardCw_Adapter_PaymentPageAdapterWrapper',
			
			
			
			Customweb_Payment_Authorization_Hidden_IAdapter::AUTHORIZATION_METHOD_NAME => 'BarclaycardCw_Adapter_HiddenAdapterWrapper',
			
			
			
			Customweb_Payment_Authorization_Server_IAdapter::AUTHORIZATION_METHOD_NAME => 'BarclaycardCw_Adapter_ServerAdapterWrapper',
			
		);
		
	}
	
	public function getConfigurationAdapterClass() {
		return 'BarclaycardCw_ConfigurationAdapter';
	}
	
	
	/**
	 * @return Customweb_Payment_BackendOperation_Adapter_IRefundAdapter
	 */
	public static function getRefundAdapter() {
		library_load_class_by_name('Customweb_Barclaycard_BackendOperation_Adapter_RefundAdapter');
		return new Customweb_Barclaycard_BackendOperation_Adapter_RefundAdapter(new BarclaycardCw_ConfigurationAdapter());
	}
	
	
	
	/**
	 * @return Customweb_Payment_BackendOperation_Adapter_ICancellationAdapter
	 */
	public static function getCancellationAdapter() {
		library_load_class_by_name('Customweb_Barclaycard_BackendOperation_Adapter_CancellationAdapter');
		return new Customweb_Barclaycard_BackendOperation_Adapter_CancellationAdapter(new BarclaycardCw_ConfigurationAdapter());
	}
	
	
	
	/**
	 * @return Customweb_Payment_BackendOperation_Adapter_ICaptureAdapter
	*/
	public static function getCapturingAdapter() {
		library_load_class_by_name('Customweb_Barclaycard_BackendOperation_Adapter_CaptureAdapter');
		return new Customweb_Barclaycard_BackendOperation_Adapter_CaptureAdapter(new BarclaycardCw_ConfigurationAdapter());
	}
	
	
	
	/**
	 * @return Customweb_Payment_Update_IAdapter
	*/
	public static function getUpdateAdapter() {
		library_load_class_by_name('____updateAdapterClass____');
		return new ____updateAdapterClass____(new BarclaycardCw_ConfigurationAdapter());
	}
	
}