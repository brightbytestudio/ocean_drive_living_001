<?php

/**
  * You are allowed to use this API in your web application.
 *
 * Copyright (C) 2013 by customweb GmbH
 *
 * This program is licenced under the customweb software licence. With the
 * purchase or the installation of the software in your application you
 * accept the licence agreement. The allowed usage is outlined in the
 * customweb software licence which can be found under
 * http://www.customweb.ch/en/software-license-agreement
 *
 * Any modification or distribution is strictly forbidden. The license
 * grants you the installation in one application. For multiuse you will need
 * to purchase further licences at http://www.customweb.com/shop.
 *
 * See the customweb software licence agreement for more details.
 *
 */

if (class_exists('WC_Payment_Gateway')) {
	class woocommerce_barclaycardcw_payment_method_proxy extends WC_Payment_Gateway {

	}
}
else {
	class woocommerce_barclaycardcw_payment_method_proxy extends woocommerce_payment_gateway {

	}
}

library_load_class_by_name('Customweb_Payment_Authorization_PaymentPage_IAdapter');
library_load_class_by_name('Customweb_Payment_Authorization_Recurring_IAdapter');
library_load_class_by_name('Customweb_Util_Url');

BarclaycardCwUtil::includeClass('BarclaycardCw_OrderContext');
BarclaycardCwUtil::includeClass('BarclaycardCw_PaymentMethodWrapper');
BarclaycardCwUtil::includeClass('BarclaycardCw_AdapterFactory');
BarclaycardCwUtil::includeClass('BarclaycardCw_Transaction');
BarclaycardCwUtil::includeClass('BarclaycardCw_TransactionContext');
BarclaycardCwUtil::includeClass('BarclaycardCw_ConfigurationAdapter');
BarclaycardCwUtil::includeClass('BarclaycardCw_RecurringTransactionContext');
BarclaycardCwUtil::includeClass('BarclaycardCw_CartOrderContext');

/**           	   	 		  		
 * This class handlers the main payment interaction with the
 * BarclaycardCw server.
 */
class BarclaycardCw_PaymentMethod extends woocommerce_barclaycardcw_payment_method_proxy implements Customweb_Payment_Authorization_IPaymentMethod{

	public $class_name;
	public $id;
	public $title;
	public $chosen;
	public $has_fields = FALSE;
	public $countries;
	public $availability;
	public $enabled = 'no';
	public $icon;
	public $description;

	/**
	 * @var BarclaycardCw_AdapterFactory
	 */
	private $adapterFactory = NULL;

	/**
	 * This is the constructor. Some options are set and the basic
	 * configuration is loaded from the database.
	 */
	public function __construct() {
		$this->class_name = substr(get_class($this), 0, 39);
		$this->id = $this->class_name;
		$this->method_title = $this->admin_title;

		// Load the form fields.
		$this->form_fields = $this->createMethodFormFields();

		// Load the settings.
		$this->init_settings();

		// Define user set variables
		$this->title = $this->settings['title'];
		$this->description = $this->settings['description'];

		// Workaround: When some setting is stored all BarclaycardCw methods are
		// deactivated. With this check we allow the storage only in case the class
		// is called from the payment_gateways tab.
		if (stristr($_SERVER['QUERY_STRING'], 'tab=payment_gateways')) {
			if (defined('WOOCOMMERCE_VERSION') && version_compare(WOOCOMMERCE_VERSION, '2.0.0') >= 0) {
				add_action( 'woocommerce_update_options_payment_gateways_' . $this->id, array( $this, 'process_admin_options' ) );
			}
			else {
				add_action('woocommerce_update_options', array(&$this, 'process_admin_options'));
			}
		}
		
		
		if ($this->getPaymentMethodConfigurationValue('enabled') == 'yes') {
			$adapter = $this->getAdapterFactory()->getAdapterByAuthorizationMethod(Customweb_Payment_Authorization_Recurring_IAdapter::AUTHORIZATION_METHOD_NAME);
			if ($adapter->isPaymentMethodSupportingRecurring($this)) {
				$this->supports = array(
					'subscriptions',
					'products',
					'subscription_cancellation',
					'subscription_reactivation',
					'subscription_suspension',
					'subscription_amount_changes',
					'subscription_date_changes',
					'product_variation'
				);
			}
		}
		add_action('scheduled_subscription_payment_' . $this->id, array(&$this, 'scheduledSubscriptionPayment'), 10, 3);
		
		
	}

	/**
	 * (non-PHPdoc)
	 * @see Customweb_Payment_Authorization_IPaymentMethod::getPaymentMethodName()
	 */
	public function getPaymentMethodName() {
		return $this->machineName;
	}

	/**
	 * (non-PHPdoc)
	 * @see Customweb_Payment_Authorization_IPaymentMethod::getPaymentMethodDisplayName()
	 */
	public function getPaymentMethodDisplayName() {
		return $this->title;
	}

	/**
	 * (non-PHPdoc)
	 * @see Customweb_Payment_Authorization_IPaymentMethod::getPaymentMethodConfigurationValue()
	 */
	public function getPaymentMethodConfigurationValue($key, $languageCode = null) {
		return $this->settings[$key];
	}
	
	public function existsPaymentMethodConfigurationValue($key, $languageCode = null) {
		if (isset($this->settings[$key])) {
			return true;
		}
		else {
			return false;
		}
	}

	public function receipt_page( $order ) {
	}

	/**
	 * This method is called when the payment is submitted.
	 *
	 * @param int $order_id
	 */
	public function process_payment($order_id) {
		global $woocommerce;

		if (strtolower($this->getPaymentMethodConfigurationValue('validation')) == 'after') {
			try {
				$this->validateByOrder($order_id);
			}
			catch(Exception $e) {
				$this->showError($e->getMessage());
			}
		}
		
		// Bugfix for WooCommerce 1.6.x to prevent the deletion of the cart, when the user goes back to the shop.
		unset($_SESSION['order_awaiting_payment']);
		unset($woocommerce->session->order_awaiting_payment);

		// Update order
		$order = BarclaycardCwUtil::loadOrderObjectById($order_id);
		$pendingStatus = 'barclaycardcw-pending';
		$order->update_status($pendingStatus, __('The customer is now in the payment process of Barclaycard.', 'woocommerce_barclaycardcw'));
		$aliasTransactionId = $this->getCurrentSelectedAlias();
		if (is_ajax()) {
			try {
				$rs = $this->getPaymentForm($order_id, $aliasTransactionId);
				if (is_array($rs)) {
					return $rs;
				}
				echo "<script type=\"text/javascript\"> jQuery('form.checkout').replaceWith(jQuery('#barclaycardcw-payment-container')); jQuery('.woocommerce-info').remove(); </script>";
				die(0);
			}
			catch(Exception $e) {
				$this->showError($e->getMessage());
			}
		}
		else {
			return array(
				'result' => 'success',
				'redirect' => BarclaycardCwUtil::getPluginUrl("payment.php", array('order_id' => $order_id, 'payment_method_class' => get_class($this), 'alias_transaction_id' => $aliasTransactionId)),
			);
		}
	}
	
	public function getCurrentSelectedAlias() {
		$aliasTransactionId = null;
		
		if (isset($_REQUEST[$this->getAliasHTMLFieldName()])) {
			$aliasTransactionId = $_REQUEST[$this->getAliasHTMLFieldName()];
		}
		else if (isset($_POST['post_data'])) {
			parse_str($_POST['post_data'], $data);
			if (isset($data[$this->getAliasHTMLFieldName()])) {
				$aliasTransactionId = $data[$this->getAliasHTMLFieldName()];
			}
		}
		
		return $aliasTransactionId;
	}
	
	private function showError($errorMessage) {
		echo '<div class="woocommerce-error">' . $errorMessage . '</div>';
		die();
	}

	public function validateByOrder($orderId) {
		$order = BarclaycardCwUtil::loadOrderObjectById($orderId);
		$orderContext = new BarclaycardCw_OrderContext($order, new BarclaycardCw_PaymentMethodWrapper($this));
		return $this->validate($orderContext);
	}
	
	public function validate(Customweb_Payment_Authorization_IOrderContext $orderContext) {
		$paymentContext = new BarclaycardCw_PaymentCustomerContext($orderContext->getCustomerId());
		
		$adapter = $this->getAdapterFactory()->getAdapterByOrderContext($orderContext);
		
		// Validate transaction
		$errorMessage = null;
		try {
			$result = $adapter->validate($orderContext, $paymentContext);
		}
		catch(Exception $e) {
			$errorMessage = $e->getMessage();
		}
		$paymentContext->persist();
		
		if ($errorMessage !== null) {
			throw new Exception($errorMessage);
		}
		return true;
	}
	
	public function getPaymentForm($orderId, $aliasTransactionId = NULL, $failedTransactionId = NULL) {

		$order = BarclaycardCwUtil::loadOrderObjectById($orderId);
		$dbTransaction = $this->newDatabaseTransaction($order);
		$transactionContext = $this->getTransactionContext($dbTransaction, $order, $aliasTransactionId);
		$adapter = $this->getAdapterFactory()->getAdapterByOrderContext($transactionContext->getOrderContext());
		
		$failedTransaction = NULL;
		if ($failedTransactionId !== NULL) {
			$dbFailedTransaction = BarclaycardCw_Transaction::loadById($failedTransactionId);
			$failedTransaction = $dbFailedTransaction->getTransactionObject();
			if ($dbFailedTransaction->getUserId() !== $order->customer_user) {
				throw new Exception("Access to this transaction is not allowed for you.");
			}
		}
		$transaction = $adapter->createTransaction($transactionContext, $failedTransaction);
		$dbTransaction->setTransactionObject($transaction);
		$dbTransaction->save();

		$vars = $adapter->getCheckoutFormVaiables($dbTransaction, $failedTransaction);
		
		// In case the adapter provides already a result, we can return this directly.
		if (isset($vars['result'])) {
			return $vars;
		}
		
		if ($failedTransaction !== NULL) {
			$vars['error_message'] = current($failedTransaction->getErrorMessages());
		}

		if (!isset($vars['template_file'])) {
			throw new Exception("No template file provided by the authorization adapter.");
		}

		$template = $vars['template_file'];
		$vars['paymentMethod'] = $this;

		// In case we use the payment page and we have no additional fields, we should directly
		// send the customer to the redirection page:
		if (empty($vars['visible_fields']) && $failedTransaction !== NULL) {
			$payment_page = Customweb_Util_Url::appendParameters(
				get_permalink(get_option('woocommerce_checkout_page_id')), 
				array('barclaycardcw_failed_transaction_id' => $failedTransactionId)
			);
			return array(
				'result' => 'success',
				'redirect' => $payment_page,
			);
		}
		
		if ($failedTransaction === NULL && $adapter instanceof Customweb_Payment_Authorization_PaymentPage_IAdapter && empty($vars['visible_fields'])) {
			return array(
				'result' => 'success',
				'redirect' => BarclaycardCwUtil::getPluginUrl("redirection.php", array('cw_transaction_id' => $dbTransaction->getTransactionId())),
			);
		}

		BarclaycardCwUtil::includeTemplateFile(str_replace('.php', '', $template), $vars);

		return null;
	}

	/**
	 * This method is invoked to check if the payment method is available for checkout.
	 */
	public function is_available() {
		
		global $woocommerce;
		
		$available = parent::is_available();
		
		if ($available !== true) {
			return null;
		}
		
		
		$woocommerce->cart->calculate_totals();
		
		$orderTotal = $woocommerce->cart->total;
		if ($orderTotal < $this->getPaymentMethodConfigurationValue('min_total')) {
			return null;
		}
		if ($this->getPaymentMethodConfigurationValue('max_total') > 0 && $this->getPaymentMethodConfigurationValue('max_total') < $orderTotal) {
			return null;
		}
		
		if (strtolower($this->getPaymentMethodConfigurationValue('validation')) == 'before') {
			$orderContext = $this->getCartOrderContext();
			if ($orderContext !== null) {
				try {
					$this->validate($orderContext);
				}
				catch(Exception $e) {
					return null;
				}
			}
		}

		return true;
	}
	
	/**
	 * @return BarclaycardCw_CartOrderContext
	 */
	private function getCartOrderContext() {
		if (!isset($_POST['post_data'])) {
			return null;
		}
		
		parse_str($_POST['post_data'], $data);
		
		return new BarclaycardCw_CartOrderContext($data, new BarclaycardCw_PaymentMethodWrapper($this));
	}
	
	public function payment_fields() {
		parent::payment_fields();
		
		if (BarclaycardCw_ConfigurationAdapter::isAliasMangerActive()) {
			$userId = get_current_user_id();
			$aliases = BarclaycardCw_Transaction::getAliasTransactions($userId, $this->getPaymentMethodName());
				
			if (count($aliases) > 0) {
				$selectedAlias = $this->getCurrentSelectedAlias();
				
				echo '<div class="barclaycardcw-alias-input-box"><div class="alias-field-description">' . __('You can choose a previous used card:', 'woocommerce_barclaycardcw') . '</div>';
				echo '<select name="' . $this->getAliasHTMLFieldName() . '">';
				echo '<option value="new"></option>';
				foreach ($aliases as $aliasTransaction) {
					echo '<option value="' . $aliasTransaction->getTransactionId() . '"';
					if ($selectedAlias == $aliasTransaction->getTransactionId()) {
						echo ' selected="selected" ';
					}
					echo '>' . $aliasTransaction->getAliasForDisplay() . '</option>';
				}
				echo '</select></div>';
			}
		}
		
	
		$orderContext = $this->getCartOrderContext();
		if ($orderContext !== null) {
			$adapter = $this->getAdapterFactory()->getAdapterByOrderContext($orderContext);
			
			$aliasTransactionObject = null;
			$selectedAlias = $this->getCurrentSelectedAlias();
			
			if ($selectedAlias !== null) {
				$aliasTransaction = BarclaycardCw_Transaction::loadById($selectedAlias);
				if ($aliasTransaction !== null && $aliasTransaction->getUserId() == get_current_user_id()) {
					$aliasTransactionObject = $aliasTransaction->getTransactionObject();
				}
			}
			
			echo $adapter->getReviewFormFields($orderContext, $aliasTransactionObject);
		}
	
	}
	
	
	public function getAliasHTMLFieldName() {
		return 'barclaycardcw_alias_' . $this->getPaymentMethodName();
	}
	

	/**
	 * @param WooComemrceOrder $order
	 * @return BarclaycardCw_Transaction
	 */
	private function newDatabaseTransaction($order) {
		$transaction = new BarclaycardCw_Transaction();
		return $transaction->setOrderId($order->id)->setUserId($order->customer_user)->setPaymentClassName(get_class($this))->save();
	}

	/**
	 *
	 * @param BarclaycardCw_Transaction $transaction
	 * @param WooCommerceOrder $order
	 * @param int $aliasTransactionId
	 * @return BarclaycardCw_TransactionContext
	 */
	private function getTransactionContext(BarclaycardCw_Transaction $transaction, $order, $aliasTransactionId = NULL) {
		return new BarclaycardCw_TransactionContext($transaction, $order, new BarclaycardCw_PaymentMethodWrapper($this), $aliasTransactionId);
	}

	public function getAdapterFactory() {
		if ($this->adapterFactory === NULL) {
			$this->adapterFactory = new BarclaycardCw_AdapterFactory();
		}

		return $this->adapterFactory;
	}

	public function processNotification(BarclaycardCw_Transaction $dbTransaction) {
		$authorizationMethod = $dbTransaction->getTransactionObject()->getAuthorizationMethod();
		$transaction = $dbTransaction->getTransactionObject();
		$adapter = $this->getAdapterFactory()->getAdapterByAuthorizationMethod($authorizationMethod);

		// Process the request and store the result
		$adapter->processAuthorization($transaction, $_REQUEST);
		$dbTransaction->save();

		// Produce the response to the client
		$this->updateOrderStatus($dbTransaction);
		$adapter->finalizeAuthorizationRequest($transaction);
	}

	protected function updateOrderStatus(BarclaycardCw_Transaction $dbTransaction) {
		global $woocommerce;

		if ($dbTransaction->getTransactionObject() === NULL) {
			throw new Exception("The transaction object is not set.");
		}

		if ($dbTransaction->getTransactionObject()->isAuthorized()) {
			// Ensure that the mail is send to the administrator
			$dbTransaction->getOrder()->update_status('pending');
				
			// Mark the transaction as completed
			$dbTransaction->getOrder()->payment_complete();
			
			
			if (class_exists('WC_Subscriptions_Order') && WC_Subscriptions_Order::order_contains_subscription($dbTransaction->getOrderId())) {
				WC_Subscriptions_Manager::activate_subscriptions_for_order($dbTransaction->getOrder());
			}
			
			
			// Remove cart
			$woocommerce->cart->empty_cart();
				
			// Update status
			$status = $dbTransaction->getTransactionObject()->getOrderStatus();
			$dbTransaction->getOrder()->update_status($status, __('Payment Notification', 'woocommerce_barclaycardcw'));
				
			// Bugfix for WooCommerce 1.6.x to ensure that the cart is emptied, when the customer returns to the shop
			$_SESSION['order_awaiting_payment'] = true;
			$woocommerce->session->order_awaiting_payment = true;
		}
		else if ($dbTransaction->getTransactionObject()->isAuthorizationFailed()) {
			$order = $dbTransaction->getOrder();
			$message = current($dbTransaction->getTransactionObject()->getErrorMessages());
			$order->add_order_note(__('Error Message: ', 'woocommerce_barclaycardcw') . $message);
			$order->cancel_order();
		}
	}
	
	
	public function scheduledSubscriptionPayment($amountToCharge, $order, $productId) {
		global $barclaycardcw_recurring_process_failure;
		$barclaycardcw_recurring_process_failure = NULL;
		try {
			$adapter = $this->getAdapterFactory()->getAdapterByAuthorizationMethod(Customweb_Payment_Authorization_Recurring_IAdapter::AUTHORIZATION_METHOD_NAME);
			
			$dbTransaction = $this->newDatabaseTransaction($order);
			$transactionContext = new BarclaycardCw_RecurringTransactionContext($dbTransaction, $order, $this, $amountToCharge, $productId);
			$transaction = $adapter->createTransaction($transactionContext);
			$dbTransaction->setTransactionObject($transaction);
			$dbTransaction->save();
			$adapter->process($transaction);
			$dbTransaction->save();
			
			if (!$transaction->isAuthorized()) {
				$message = current($transaction->getErrorMessages());
				throw new Exception($message);
			}
			
			WC_Subscriptions_Manager::process_subscription_payments_on_order($order);
		}
		catch(Exception $e) {
			$errorMessage = __('Subscription Payment Failed with error:', 'woocommerce_barclaycardcw') . $e->getMessage();
			$barclaycardcw_recurring_process_failure = $errorMessage;
			$order->add_order_note($errorMessage);
			WC_Subscriptions_Manager::process_subscription_payment_failure_on_order($order, $product_id);
		}
	}
	

	/**
	 * This method generates a HTML form for each payment method.
	 */
	public function createMethodFormFields() {
		return array(
			'enabled' => array(
				'title' => __('Enable/Disable', 'woothemes'),
				'type' => 'checkbox',
				'label' => sprintf(__('Enable %s', 'woocommerce_barclaycardcw'), $this->admin_title),
				'default' => 'no'
			),
			'title' => array(
				'title' => __('Title', 'woothemes'),
				'type' => 'text',
				'description' => __('This controls the title which the user sees during checkout.', 'woothemes'),
				'default' => __($this->title, 'woocommerce_barclaycardcw')
			),
			'description' => array(
				'title' => __('Description', 'woothemes'),
				'type' => 'textarea',
				'description' => __('This controls the description which the user sees during checkout.', 'woothemes'),
				'default' => sprintf(__("Pay with %s over the interface of Barclaycard.", 'woocommerce_barclaycardcw'), $this->title)
			),
			'min_total' => array(
				'title' => __('Minimal Order Total', 'woocommerce_barclaycardcw'),
				'type' => 'text',
				'description' => __('Set here the minimal order total for which this payment method is available. If it is set to zero, it is always available.', 'woocommerce_barclaycardcw'),
				'default' => 0,
			),
			'max_total' => array(
				'title' => __('Maximal Order Total', 'woocommerce_barclaycardcw'),
				'type' => 'text',
				'description' => __('Set here the maximal order total for which this payment method is available. If it is set to zero, it is always available.', 'woocommerce_barclaycardcw'),
				'default' => 0,
			),
			'validation' => array(
				'title' => __('Validation', 'woocommerce_barclaycardcw'),
				'type' => 'select',
				'description' => __('The validation of the payment can be carried out at various points during the order process. The validation can be controlled with this option.', 'woocommerce_barclaycardcw'),
				'options' => array(
					'before' => __('Before selection of payment method', 'woocommerce_barclaycardcw'),
					'after' => __('After selection of payment method', 'woocommerce_barclaycardcw'),
					'authorization' => __('During the authorization', 'woocommerce_barclaycardcw'),
				),
				'default' => 'after',
			),
		);
	}

	function generate_select_html ( $key, $data ) {
		// We need to override this method, because we need to get
		// the order status, after we defined the form fields. The
		// terms are not accessible before.
		if (isset($data['is_order_status']) && $data['is_order_status'] == true) {
			$terms = get_terms('shop_order_status', array('hide_empty' => 0, 'orderby' => 'id'));

			$statuses = array();
			
			if (!isset($data['options']) || !is_array($data['options'])) {
				$data['options'] = array();
			}
			
			foreach ($data['options'] as $k => $value) {
				$statuses[$k] =  __($value, 'woocommerce_barclaycardcw');
			}

			foreach ($terms as $term) {
				$statuses[$term->slug] = $term->name;
			}
			$data['options'] = $statuses;
		}
		return parent::generate_select_html($key, $data);
	}


	/**
	 * Generate the HTML output for the settings form.
	 */
	public function admin_options() {
		$output = '<h3>' . __($this->admin_title, 'woocommerce_barclaycardcw') . '</h3>';
		$output .= '<p>' . __('The configuration values for Barclaycard can be set under:', 'woocommerce_barclaycardcw') .
		' <a href="options-general.php?page=woocommerce-barclaycardcw">' . __('Barclaycard Settings', 'woocommerce_barclaycardcw') . '</a>' .
		'</p>';

		$output .= '<table class="form-table">';

		echo $output;

		$this->generate_settings_html();

		echo '</table>';
	}

	
	public static function refundTransaction(BarclaycardCw_Transaction $dbTransaction, $amount, $close) {
		$adapter = BarclaycardCw_AdapterFactory::getRefundAdapter();
		$items = Customweb_Util_Invoice::getItemsByReductionAmount($dbTransaction->getTransactionObject()->getTransactionContext()->getOrderContext()->getInvoiceItems(), 
					$amount,
					$dbTransaction->getTransactionObject()->getCurrencyCode()
		);
		$rs = $adapter->partialRefund($dbTransaction->getTransactionObject(), $items, $close);
		$dbTransaction->save();
		return $rs;
	}
	
	public static function cancelTransaction(BarclaycardCw_Transaction $dbTransaction) {
		$adapter = BarclaycardCw_AdapterFactory::getCancellationAdapter();
		$rs = $adapter->cancel($dbTransaction->getTransactionObject());
		$dbTransaction->save();
		
		$status = $dbTransaction->getTransactionObject()->getOrderStatus();
		$dbTransaction->getOrder()->update_status($status, __('Payment Cancelled', 'woocommerce_barclaycardcw'));
		return $rs;
	}
	
	public static function captureTransaction(BarclaycardCw_Transaction $dbTransaction, $amount, $close) {
		$adapter = BarclaycardCw_AdapterFactory::getCapturingAdapter();
		$items = Customweb_Util_Invoice::getItemsByReductionAmount($dbTransaction->getTransactionObject()->getTransactionContext()->getOrderContext()->getInvoiceItems(),
				$amount,
				$dbTransaction->getTransactionObject()->getCurrencyCode()
		);
		$rs = $adapter->partialCapture($dbTransaction->getTransactionObject(), $items, $close);
		$dbTransaction->save();
		
		$status = $dbTransaction->getTransactionObject()->getOrderStatus();
		$dbTransaction->getOrder()->update_status($status, __('Payment Captured', 'woocommerce_barclaycardcw'));
		return $rs;
	}

}
