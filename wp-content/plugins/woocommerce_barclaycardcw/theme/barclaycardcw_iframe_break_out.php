<script type="text/javascript"> 
	top.location.href = '<?php echo $url; ?>';
</script>

<noscript>
	<a class="button barclaycardcw-continue-button" href="<?php echo $url; ?>" target="_top"><?php echo __('Continue', 'woocommerce_barclaycardcw'); ?></a>
</noscript>
