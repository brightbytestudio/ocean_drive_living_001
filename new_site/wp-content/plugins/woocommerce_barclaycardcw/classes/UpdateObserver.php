<?php
/**
  * You are allowed to use this API in your web application.
 *
 * Copyright (C) 2013 by customweb GmbH
 *
 * This program is licenced under the customweb software licence. With the
 * purchase or the installation of the software in your application you
 * accept the licence agreement. The allowed usage is outlined in the
 * customweb software licence which can be found under
 * http://www.customweb.ch/en/software-license-agreement
 *
 * Any modification or distribution is strictly forbidden. The license
 * grants you the installation in one application. For multiuse you will need
 * to purchase further licences at http://www.customweb.com/shop.
 *
 * See the customweb software licence agreement for more details.
 *
 * @category	Local
 * @package	Customweb_BarclaycardCw
 * @link		http://www.customweb.ch
 */

BarclaycardCwUtil::includeClass('BarclaycardCw_Log');

class Customweb_BarclaycardCw_Model_UpdateObserver{
	
	/**
	 * This function is called by the WP cron method
	 */
	public function pullAllUpdates() {
		
		$this->processBatchTransaction();
		$this->processUpdatableTransaction();
		
	}
		
	protected function processUpdatableTransaction(){
		$updateAdapter = BarclaycardCw_AdapterFactory::getUpdateAdapter();
		$updatableTransactions = BarclaycardCw_Transaction::getUpdatableTransactions();
		foreach ($updatableTransactions as $dbTransaction) {
			try {
				$transaction = $dbTransaction->getTransactionObject();
				$updateAdapter->pullTransactionUpdate($transaction, BarclaycardCwUtil::getBackendOperationAdapterFactory());
				$dbTransaction->setTransactionObject($transaction);
				$dbTransaction->save();
			}
			catch (Exception $e) {
				$message = "Updating of transaction with id ".$dbTransaction->getTransactionId()." failed. \nReason: ".$e->getMessage();
				BarclaycardCw_Log::add($message);
			}
		}
	}
	
	protected function processBatchTransaction(){
		$updateAdapter = BarclaycardCw_AdapterFactory::getUpdateAdapter();
		$lastSuccessFullUpdate = $this->getLastSuccessfullUpdate();
		$now = new DateTime();
		try{
			$transactionsToUpdate = $updateAdapter->batchUpdate($lastSuccessFullUpdate);
			foreach($transactionsToUpdate as $paymentId => $parameters ) {
				try{
					$dbTransaction = BarclaycardCw_Transaction::getTransactionByPaymentId($paymentId);
					$transaction = $dbTransaction->getTransactionObject();
					$updateAdapter->processTransactionUpdate($transaction, $parameters, BarclaycardCwUtil::getBackendOperationAdapterFactory());
					$dbTransaction->setTransactionObject($transaction);
					$dbTransaction->save();
				}
				catch (Exception $e) {
					$message = "Updating of transaction with id ".$dbTransaction->getTransactionId()." failed. \nReason: ".$e->getMessage();
					BarclaycardCw_Log::add($message);
				}
			}
			update_option('woocommerce_____moduleCamel_____lastSuccessfullPull',$now->format('Y-m-d H:i:s'));
		}
		catch( Exception $e) {
			$message = "Batch update failed.\nReason: ".$e->getMessage();
			BarclaycardCw_Log::add($message); 
		}	
	}
	
	private function getLastSuccessfullUpdate() {
		$lastUpdate = null;
		try{
			$lastUpdateString = get_option('woocommerce_____moduleCamel_____lastSuccessfullPull');
			if($lastUpdateString != null){
				return new DateTime($lastUpdateString);
			}
		}
		catch(Exception $e){
			
		}
		return $lastUpdate;
		
	}
}