<?php 
/**
  * You are allowed to use this API in your web application.
 *
 * Copyright (C) 2013 by customweb GmbH
 *
 * This program is licenced under the customweb software licence. With the
 * purchase or the installation of the software in your application you
 * accept the licence agreement. The allowed usage is outlined in the
 * customweb software licence which can be found under
 * http://www.customweb.ch/en/software-license-agreement
 *
 * Any modification or distribution is strictly forbidden. The license
 * grants you the installation in one application. For multiuse you will need
 * to purchase further licences at http://www.customweb.com/shop.
 *
 * See the customweb software licence agreement for more details.
 *
 */

library_load_class_by_name('Customweb_Payment_Authorization_Ajax_AbstractWrapper');
require_once 'IAdapter.php';

class BarclaycardCw_Adapter_AjaxAdapterWrapper extends Customweb_Payment_Authorization_Ajax_AbstractWrapper implements BarclaycardCw_Adapter_IAdapter
{
	
	public function getCheckoutFormVaiables(BarclaycardCw_Transaction $dbTransaction, $failedTransaction) {
		
		$transaction = $dbTransaction->getTransactionObject();
		
		$ajaxScriptUrl = $this->getAjaxFileUrl($transaction);
		$callbackFunction = $this->getJavaScriptCallbackFunction($transaction);
		$visibleFormFields = $this->getVisibleFormFields(
			$transaction->getTransactionContext()->getOrderContext(), 
			$transaction->getTransactionContext()->getAlias(),
			$failedTransaction,
			$dbTransaction->getTransactionObject()->getTransactionContext()->getPaymentCustomerContext()
		);
		$dbTransaction->save();
		
		if (isset($_REQUEST['barclaycardcw-ajax-authorization'])) {
			return array(
				'result' => 'success',
				'ajaxScriptUrl' => $ajaxScriptUrl,
				'submitCallbackFunction' => $callbackFunction,
			);
		}
		
		
		$html = '';
		if ($visibleFormFields !== null && count($visibleFormFields) > 0) {
			$renderer = new Customweb_Form_Renderer();
			$renderer->setCssClassPrefix('barclaycardcw-');
			$html = $renderer->renderElements($visibleFormFields);
		}
		
		return array(
			'visible_fields' => $html,
			'ajaxScriptUrl' => $ajaxScriptUrl,
			'submitCallbackFunction' => $callbackFunction,
			'template_file' => 'payment_confirmation_ajax',
		);
	}
	
	public function getReviewFormFields(Customweb_Payment_Authorization_IOrderContext $orderContext, $aliasTransaction) {
	
		if (BarclaycardCw_ConfigurationAdapter::isReviewFormInputActive()) {
			$paymentContext = new BarclaycardCw_PaymentCustomerContext($orderContext->getCustomerId());
			$fields = $this->getVisibleFormFields($orderContext, $aliasTransaction, null, $paymentContext);
			$paymentContext->persist();
				
			if ($fields !== null && count($fields) > 0) {
				$renderer = new Customweb_Form_Renderer();
				$renderer->setRenderOnLoadJs(false);
				$renderer->setNameSpacePrefix('barclaycardcw_' . $orderContext->getPaymentMethod()->getPaymentMethodName());
				$renderer->setCssClassPrefix('barclaycardcw-');
				return '<div class="barclaycardcw-ajax-authorization">' . $renderer->renderElements($fields) . '</div>';
			}
		}
	
		return '';
	}
	
	
}