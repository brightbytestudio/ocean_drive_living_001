<?php

/**
 * Plugin Name: WooCommerce BarclaycardCw
 * Plugin URI: http://www.customweb.ch
 * Description: This plugin adds the BarclaycardCw payment gateway to your WooCommerce.
 * Version: 2.1.19
 * Author: customweb GmbH
 * Author URI: http://www.customweb.ch
 */

/**
 * You are allowed to use this API in your web application.
 *
 * Copyright (C) 2013 by customweb GmbH
 *
 * This program is licenced under the customweb software licence. With the
 * purchase or the installation of the software in your application you
 * accept the licence agreement. The allowed usage is outlined in the
 * customweb software licence which can be found under
 * http://www.customweb.ch/en/software-license-agreement
 *
 * Any modification or distribution is strictly forbidden. The license
 * grants you the installation in one application. For multiuse you will need
 * to purchase further licences at http://www.customweb.com/shop.
 *
 * See the customweb software licence agreement for more details.
 *
*/

// Make sure we don't expose any info if called directly
if (!function_exists('add_action')) {
	echo "Hi there!  I'm just a plugin, not much I can do when called directly.";
	exit;
}

// Load Language Files
load_plugin_textdomain('woocommerce_barclaycardcw', false, basename( dirname( __FILE__ ) ) . '/translations' );

require_once dirname( __FILE__ ) . '/lib/loader.php';
require_once 'BarclaycardCwUtil.php';

// Add translation Adapter
BarclaycardCwUtil::includeClass('TranslationResolver');

// Get all admin functionality
require_once BarclaycardCwUtil::getBasePath() . '/admin.php';

register_activation_hook( __FILE__, array('BarclaycardCwUtil', 'installPlugin'));
add_filter('woocommerce_payment_gateways', array('BarclaycardCwUtil', 'addPaymentMethods'));

if (!is_admin()) {
	function woocommerce_barclaycardcw_add_frontend_css() {
		wp_register_style('woocommerce_barclaycardcw_frontend_styles', plugins_url('assets/frontend.css', __FILE__));
		wp_enqueue_style('woocommerce_barclaycardcw_frontend_styles');
		
		wp_register_script(
			'barclaycardcw_frontend_script',
			plugins_url('assets/frontend.js', __FILE__),
			array('jquery')
		);
		wp_enqueue_script( 'barclaycardcw_frontend_script' );
	}
	add_action('wp_enqueue_scripts', 'woocommerce_barclaycardcw_add_frontend_css');
}

// Log Errors
function woocommerce_barclaycardcw_add_errors() {
	if (isset($_GET['barclaycardcw_failed_transaction_id'])) {
		global $woocommerce;
		BarclaycardCwUtil::includeClass('BarclaycardCw_Transaction');
		$dbTransaction = BarclaycardCw_Transaction::loadById($_GET['barclaycardcw_failed_transaction_id']);
		$woocommerce->add_error(current($dbTransaction->getTransactionObject()->getErrorMessages()));
	}
	
}
add_action('init', 'woocommerce_barclaycardcw_add_errors');

add_action('woocommerce_before_checkout_billing_form', array('BarclaycardCwUtil', 'actionBeforeCheckoutBillingForm'));
add_action('woocommerce_before_checkout_shipping_form', array('BarclaycardCwUtil', 'actionBeforeCheckoutShippingForm'));


//Cron Functions to pull PSP update
register_activation_hook( __FILE__, array('BarclaycardCwUtil', 'activatePullCron'));
register_deactivation_hook(__FILE__, array('BarclaycardCwUtil', 'deactivatePullCron'));

add_filter( 'cron_schedules', array('BarclaycardCwUtil', 'createPullCronInterval'));
add_action( 'BarclaycardCwPullCronHook', array('BarclaycardCwUpdateObserver', 'pullAllUpdates'));
