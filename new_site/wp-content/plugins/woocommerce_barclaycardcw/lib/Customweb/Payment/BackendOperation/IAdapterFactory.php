<?php 

interface Customweb_Payment_BackendOperation_IAdapterFactory {

	
	public function setConfigurationAdapter(Customweb_Payment_IConfigurationAdapter $configurationAdapter);
	
	/**
	 * Returns the set configuration adapter.
	 * 
	 * @return Customweb_Payment_IConfigurationAdapter
	 */
	public function getConfigurationAdapter();
	
	/**
	 * Returns the adapter for cancellations.
	 * @return Customweb_Payment_BackendOperation_Adapter_ICancellationAdapter
	 */
	public function getCancellationAdapter();
	
	/**
	 * Returns the adapter for refunds.
	 * 
	 * @return Customweb_Payment_BackendOperation_Adapter_IRefundAdapter
	 */
	public function getRefundAdapter();
	
	/**
	 * Returns the adapter for captures.
	 * 
	 * @return Customweb_Payment_BackendOperation_Adapter_ICaptureAdapter
	 */
	public function getCaptureAdapter();
}