<?php

/*************************** LOAD THE BASE CLASS ********************************/
if(!class_exists('WP_List_Table')){
    require_once( ABSPATH . 'wp-admin/includes/class-wp-list-table.php' );
}

/************************** CREATE A PACKAGE CLASS ******************************/
class Zone_List_Table extends WP_List_Table {
    
    function __construct(){
        global $status, $page;
                
        //Set parent defaults
        parent::__construct( array(
            'singular'  => 'zone', 
            'plural'    => 'zones',
            'ajax'      => false
        ) );
        
    }
    /**
     * Add extra markup in the toolbars before or after the list
     * @param string $which, helps you decide if you add the markup after (bottom) or before (top) the list
     */
    function extra_tablenav( $which ) {
        if ( $which == "top" ){
            //The code that goes before the table is here
            echo"<span style='line-height:32px;'>To manage the shipping rates for these zones, visit the <a href=\"" . get_bloginfo( 'wpurl' ) . "/wp-admin/admin.php?page=woocommerce_settings&tab=shipping&section=BE_Table_Rate_Shipping\">Table Rate Shipping</a> settings page.</span>";
        }
        if ( $which == "bottom" ){
            //The code that goes after the table is there
            echo"<span style='line-height:32px;'>Drag and drop the table rows to sort the zones by their priority, lowest - highest. Click the <strong>Save Changes</strong> button when finished.</span>";
        }
    }
    function column_default($item, $column_name){
        global $woocommerce;

        switch($column_name){
            case 'status':
                $url = wp_nonce_url( admin_url( 'admin-ajax.php?action=wtrs-zone-enabled&zone_id=' . $item->zone_id ), 'woocommerce-settings' );
                $return_image = '<p style="text-align:center"><a href="' . $url . '" title="'. __( 'Toggle featured', 'woocommerce' ) . '">';
                if ($item->zone_enabled == '1')
                    $return_image .=  '<img src="' . $woocommerce->plugin_url() . '/assets/images/success.png" alt="yes" />';
                else
                    $return_image .=  '<img src="' . $woocommerce->plugin_url() . '/assets/images/success-off.png" alt="no" />';
                return $return_image . "</a></p>";
            case 'order':
                return $item->zone_order;
            default:
                return "Data Could Not Be Found";
        }
    }
    
    function column_title($item){
        
        //Build row actions
        $actions = array(
            'edit'      => sprintf('<a href="?page=%s&tab=shipping_zones&action=%s&zone=%s">Edit</a>',$_REQUEST['page'],'edit',$item->zone_id),
            'delete'    => sprintf('<a href="?page=%s&tab=shipping_zones&action=%s&zone=%s">Delete</a>',$_REQUEST['page'],'delete',$item->zone_id),
        );
        
        //Return the title contents
        return sprintf('<h3 style="margin:0;">%1$s</h3><small>ID: %2$s</small><br /><i>%3$s</i><br />%4$s</p>',
            /*$1%s*/ $item->zone_title,
            /*$2%s*/ $item->zone_id,
            /*$3%s*/ $item->zone_description,
            /*$4%s*/ $this->row_actions($actions)
        );
    }
    
    function column_locations($item){
        global $woocommerce;

        $countries = $woocommerce->countries->countries;
        $states = $woocommerce->countries->states;
        switch ($item->zone_type) {
            case 'everywhere':
                return 'Everywhere';
            case 'countries':
                $return_val = "";
                $i = 0;
                $countries_abbr = explode(',', sanitize_text_field($item->zone_country));
                $cur_country = "";
                foreach ($countries_abbr as $value) {
                    $country_state = explode(':',$value);
                    $country = $country_state[0];
                    if($cur_country != $country_state[0]) {
                        if(isset($country_state[1]) && $country_state[1] != "" && $i > 0) $return_val .= "<br />";
                        if($i > 0) $return_val .= "<br />";
                        $cur_country = $country_state[0];
                        $return_val .= "<strong>".$countries[$country]."</strong><br />";
                        $i = 0;
                    }
                    if($i > 0) $return_val .= ", ";
                    if(count($country_state) > 1) {
                        $return_val .= $states[$country][$country_state[1]];
                        $i++;
                    }
                }
                return $return_val;
            case 'postal':
                $country_state = explode(':',$item->zone_country);
                $country = $country_state[0];
                $state = (count($country_state) > 1) ? $states[$country][$country_state[1]] . " &raquo; " : "";
                return "<strong>".$state."".$countries[$country]."</strong><br />".sanitize_text_field($item->zone_postal);
            default:
                return __("Data Could Not Be Found","woocommerce");
        }
    }


    /** ************************************************************************
     * @see WP_List_Table::::single_row_columns()
     * @param array $item A singular item (one full row's worth of data)
     * @return string Text to be placed inside the column <td> (movie title only)
     **************************************************************************/
    function column_cb($item){
        return sprintf(
            '<input type="checkbox" name="%1$s[]" value="%2$s" />',
            /*$1%s*/ $this->_args['singular'],  //Let's simply repurpose the table's singular label ("movie")
            /*$2%s*/ $item->zone_id                //The value of the checkbox should be the record's id
        );
    }
    
    function get_columns(){
        $columns = array(
            'cb'        => '<input type="checkbox" />', //Render a checkbox instead of text
            'title'     => __('Zone Title','woocommerce'),
            'locations'    => __('Locations','woocommerce'),
            'status'  => "<p style=\"text-align:center;margin:0;\">".__('Status','woocommerce')."</p>"
            // 'order'  => __('Priority Order','woocommerce')
        );
        return $columns;
    }

    function get_bulk_actions() {
        $actions = array(
            'delete'    => 'Delete'
        );
        return $actions;
    }
    
    function process_bulk_action() {
        global $wpdb;

        //Detect when a bulk action is being triggered...
        if( 'delete'===$this->current_action() ) {
            if(isset($_GET['zone']) && is_numeric($_GET['zone'])) :
                $zone_verify = $wpdb->get_var("SELECT zone_id FROM ".$wpdb->prefix."woocommerce_shipping_zones WHERE zone_id='".$_GET['zone']."'");
                if($zone_verify == '') 
                    echo "<div class=\"error\"><p>".__('A zone with the ID provided does not exist', 'woocommerce').".</p></div>";
                else {
                    $zone_title = $wpdb->get_var("SELECT zone_title FROM ".$wpdb->prefix."woocommerce_shipping_zones WHERE zone_id='".$_GET['zone']."'");
                    $wpdb->query($wpdb->prepare("DELETE FROM ".$wpdb->prefix."woocommerce_shipping_zones WHERE zone_id='%s'",$_GET['zone']));
                    echo "<div class=\"updated\"><p>".__("The zone titled <strong>".$zone_title."</strong> has been deleted",'woocommerce').".</p></div>";
                }
            endif;
        }
    }
    
    function prepare_items() {
    global $wpdb, $_wp_column_headers;
    $screen = get_current_screen();

    /* -- Preparing your query -- */
        $query = "SELECT * FROM ".$wpdb->prefix."woocommerce_shipping_zones ORDER BY zone_order ASC";
        $totalitems = $wpdb->query($query); 
        $perpage = 9999;

    /* -- Register the Columns -- */
        $columns = $this->get_columns();
        $_wp_column_headers[$screen->id]=$columns;

    /* -- Fetch the items -- */

        $columns = $this->get_columns();
        $hidden = array();
        
        $this->_column_headers = array($columns, $hidden, false);
        $this->process_bulk_action();
        $this->items = $wpdb->get_results($query);
        $data = $this->items;
        
        $current_page = $this->get_pagenum();
        $total_items = count($data);
        $data = array_slice($data,(($current_page-1)*$perpage),$perpage);
        $this->items = $data;
        
    /* -- Pagination parameters -- */

        $paged = !empty($_GET["paged"]) ? mysql_real_escape_string($_GET["paged"]) : '';
        if(empty($paged) || !is_numeric($paged) || $paged<=0 ){ $paged=1; }
        $totalpages = ceil($totalitems/$perpage);
        if(!empty($paged) && !empty($perpage)){
            $offset=($paged-1)*$perpage;
            $query.=' LIMIT '.(int)$offset.','.(int)$perpage;
        }

    /* -- Register the pagination -- */
        $this->set_pagination_args( array(
            "total_items" => $totalitems,
            "total_pages" => $totalpages,
            "per_page" => $perpage,
        ) );

    }

    function single_row( $item ) {
        static $row_class = '';
        $row_class = ( $row_class == '' ? ' class="alternate"' : '' );

        echo '<tr' . $row_class . '>';
        echo '<input type="hidden" name="zone_id[]" value="'.$item->zone_id.'" />';
        echo $this->single_row_columns( $item );
        echo '</tr>';
    }


    function tt_render_list_page(){
        
        //Create an instance of our package class...
        $zoneListTable = new Zone_List_Table();
        //Fetch, prepare, sort, and filter our data...
        $zoneListTable->prepare_items();
        
        ?>
        <div class="wrap woocommerce">
            
            <div id="icon-users" class="icon32"><br/></div>
            <h2>Shipping Zones <a href="?page=<?php echo $_REQUEST['page']; ?>&tab=shipping_zones&action=new" class="add-new-h2">Add New</a></h2>

            <?php if(isset($_GET['upgrade'])) : ?>
            <div class="updated">
                <h4>Thank You for Updating to Version 3.1 of the Table Rate Plugin for WooCommerce!</h4>
                <p>This version includes an update to zone support, giving the ability to add the same settings to multiple countries and areas without the extra table rows. If you have used a previous version of this plugin, the upgrade feature has created new zones for you in order to preserve your original settings. Please take a moment to review them below and make changes necessary.</p>
                <p>For further instruction on installing and using this plugin and its new features, please refer to the documention found in the zip file you downloaded from CodeCanyon.</p>
            </div>
            <?php elseif(isset($_GET['repair_complete'])) : ?>
            <div class="updated"><p>The shipping zone database has been reset. Try adding a new zone to test that it is working order now.</p></div>
            <?php endif; ?>
            
            </form>
            <!-- Forms are NOT created automatically, so you need to wrap the table in one to use features like bulk actions -->
            <form id="zones-filter" method="post" action="">
                <!-- For plugins, we also need to ensure that the form posts back to our current page -->
                <input type="hidden" name="page" value="<?php echo $_REQUEST['page'] ?>" />
                <!-- Now we can render the completed list table -->
                <?php $zoneListTable->display() ?>
                <?php wp_nonce_field( 'woocommerce-settings', '_wpnonce', true, true ); ?>
                <p class="submit">
                    <input name="save" class="button-primary" type="submit" value="Save changes" />
                    <input type="hidden" name="subtab" id="last_tab" />
                </p>
            </form>
            <div style="float:right"><a href="?page=<?php echo $_REQUEST['page']; ?>&tab=shipping_zones&action=repair">Zones Not Working? Repair DB</a></div>
            
        </div>
        <script>
            jQuery(function() {
                var fixHelperModified = function(e, tr) {
                    var $originals = tr.children();
                    var $helper = tr.clone();
                    $helper.children().each(function(index)
                    {
                      jQuery(this).width($originals.eq(index).width())
                    });
                    return $helper;
                };
                jQuery("#the-list").sortable({
                    helper: fixHelperModified
                }).disableSelection();
            });
        </script>
        <?php
    }


    function tt_render_edit_page($item=''){
        global $woocommerce, $wpdb;
        $method = $_GET['action'];
        $zone_fields = array();
        $allowed_countries = $woocommerce->countries->get_allowed_countries();
        asort( $allowed_countries );
        
        ?>
        <div class="wrap woocommerce">
            
            <div id="icon-users" class="icon32"><br/></div>
            <h2><?php if($method == 'edit') : ?>Edit<?php else : ?>Add<?php endif;?> Shipping Zone <a href="?page=<?php echo $_REQUEST['page']; ?>&tab=shipping_zones&action=new" class="add-new-h2">Add New</a></h2>

    <?php 
        if($method == 'edit') : 
            $zoneID = (int) $_GET['zone'];
            if(!is_numeric($zoneID) || $zoneID == 0) { echo "<p>A valid zone ID must be supplied.</p>"; return; }

            $selZone = "SELECT * FROM ".$wpdb->prefix."woocommerce_shipping_zones WHERE zone_id=$zoneID";
            $totalitems = $wpdb->query($selZone);
            if($totalitems != 1) { echo "<p>Sorry, a zone with the given ID could not be found.</p>"; return; }

            $zone_fields = $wpdb->get_row($selZone,'ARRAY_A');
        endif;
    ?>
            
            <!-- Forms are NOT created automatically, so you need to wrap the table in one to use features like bulk actions -->
            <form id="zone-editor" method="post" action="">
                <!-- For plugins, we also need to ensure that the form posts back to our current page -->
                <input type="hidden" name="page" value="<?php echo $_REQUEST['page'] ?>" />
                <input type="hidden" name="zone_id" value="<?php echo $zoneID ?>" />
                <!-- Now we can render the completed list table -->
                <h3><?php echo __('Zone Details', 'woocommerce'); ?></h3>
                <table class="form-table">
                <tr valign="top">
                    <th scope="row" class="titledesc"><label><?php echo __('Enabled', 'woocommerce'); ?></label></th>
                    <td><input type="checkbox" name="zone_enabled" <?php if($zone_fields['zone_enabled'] == 1) echo "checked=checked"; ?> /> Enable/Disable the use of this zone</td>
                    </tr>
                <tr valign="top">
                    <th scope="row" class="titledesc"><label><?php echo __('Title', 'woocommerce'); ?></label></th>
                    <td><input type="text" name="zone_title" style="min-width:450px;" value="<?php if(isset($zone_fields['zone_title'])) echo $zone_fields['zone_title']; ?>" /></td>
                    </tr>
                <tr valign="top">
                    <th scope="row" class="titledesc"><label><?php echo __('Description', 'woocommerce'); ?></label></th>
                    <td><textarea style="width:450px;height:75px" name="zone_description"><?php if(isset($zone_fields['zone_description'])) echo $zone_fields['zone_description']; ?></textarea><br />This is an optional field to provide admins a brief description of this zone. This will NOT appear on any shop page</td>
                    </tr>
                <tr valign="top">
                    <th scope="row" class="titledesc"><label><?php echo __('Type', 'woocommerce'); ?></label></th>
                    <td><select style="min-width:450px;" class="chosen_select" id="zone_type" name="zone_type">
                        <option value="everywhere"<?php if ($zone_fields['zone_type'] == 'everywhere') echo " selected=selected"; ?>><?php echo __('Everywhere', 'woocommerce'); ?></option>
                        <option value="countries"<?php if ($zone_fields['zone_type'] == 'countries') echo " selected=selected"; ?>><?php echo __('Countries', 'woocommerce'); ?></option>
                        <option value="postal"<?php if ($zone_fields['zone_type'] == 'postal') echo " selected=selected"; ?>><?php echo __('Postal Code', 'woocommerce'); ?></option>
                    </select></td>
                    </tr>
                </table>
                <h3><?php echo __('Locations', 'woocommerce'); ?></h3>
                <table id="location_everywhere" class="form-table" style="display:none">
                <tr valign="top">
                    <th scope="row" class="titledesc"><label><?php echo __('Countries', 'woocommerce'); ?></label></th>
                    <td>All Allowed Countries as set on the 'General' tab </td>
                    </tr>
                </table>
                <table id="location_countries" class="form-table" style="display:none">
                <tr valign="top">
                    <th scope="row" class="titledesc"><label><?php echo __('Countries', 'woocommerce'); ?></label></th>
                    <td class="forminp">
                            <select multiple="multiple" name="location_countries[]" style="width:450px;" data-placeholder="<?php _e( 'Choose countries&hellip;', 'woocommerce' ); ?>" title="Country" class="chosen_select">
                                <?php
                                    if ( $allowed_countries )
                                        $selections = explode(',', $zone_fields['zone_country']);
                                        foreach ( $allowed_countries as $key => $val ) {
                                            echo '<option value="'.$key.'" ' . selected( in_array( $key, $selections ), true, false ).'>' . $val . '</option>';
                                            $allowed_states = $woocommerce->countries->get_states($key);
                                            if(count($allowed_states) > 0) {
                                                foreach ($allowed_states as $skey => $sval) {
                                                    echo '<option value="'.$key.':'.$skey.'" ' . selected( in_array( $key.':'.$skey, $selections ), true, false ).'>&#009;' . $val . ' &mdash; ' . $sval . '</option>';
                                                }
                                            }
                                        }
                                ?>
                            </select>
                        </td>
                    </tr>
                </table>
                <table id="location_postal_code" class="form-table" style="display:none">
                <tr valign="top">
                    <th scope="row" class="titledesc"><label><?php echo __('Country', 'woocommerce'); ?></label></th>
                    <td class="forminp"><select name="location_country" style="width:450px;" data-placeholder="<?php _e( 'Choose a country&hellip;', 'woocommerce' ); ?>" title="Country" class="chosen_select">
<?php

                    if ( $allowed_countries )
                        foreach ( $allowed_countries as $key => $val ) {
                            echo '<option value="'.$key.'" ' . selected( $key, $zone_fields['zone_country'], true, false ).'>' . $val . '</option>';
                            $allowed_states = $woocommerce->countries->get_states($key);
                            if(count($allowed_states) > 0) {
                                foreach ($allowed_states as $skey => $sval) {
                                    echo '<option value="'.$key.':'.$skey.'" ' . selected( $key.':'.$skey, $zone_fields['zone_country'], true, false ).'>&#009;' . $val . ' &mdash; ' . $sval . '</option>';
                                }
                            }
                        }
?>
                    </select></td>
                    </tr>
                <tr valign="top">
                    <th scope="row" class="titledesc"><label><?php echo __('Postal Codes', 'woocommerce'); ?></label></th>
                    <td><textarea style="width:450px;height:75px" name="location_codes"><?php if(isset($zone_fields['zone_postal'])) echo sanitize_text_field($zone_fields['zone_postal']); ?></textarea><br ?>
                        <strong>-</strong> <small><?php _e('is used to separate two postal codes in a range (numerical codes ONLY)','woocommerce'); ?></small></br />
                        <strong>*</strong> <small><?php _e('is a wildcard used to representent multiple characters/numbers','woocommerce'); ?></small></br />
                        <strong>^</strong> <small><?php _e('is used to denote postal codes or ranges to be excluded','woocommerce'); ?></small></br /></td>
                    </tr>
                </table>
            <script type="text/javascript">
                jQuery(function() {
                    // Load default frame
                    jQuery(window).load(function () {

                    var e = document.getElementById("zone_type");
                    var method_sel = e.options[e.selectedIndex].value;
                    if(method_sel=='everywhere') document.getElementById('location_everywhere').style.display='table-row';
                    if(method_sel=='countries') document.getElementById('location_countries').style.display='table-row';
                    if(method_sel=='postal') document.getElementById('location_postal_code').style.display='table-row';

                    return false;
                    });
                    // Event Handler for Change of Shipping Method
                    jQuery('#zone_type').change(function(){

                    document.getElementById('location_everywhere').style.display='none';
                    document.getElementById('location_countries').style.display='none';
                    document.getElementById('location_postal_code').style.display='none';

                    var e = document.getElementById("zone_type");
                    var method_sel = e.options[e.selectedIndex].value;
                    if(method_sel=='everywhere') document.getElementById('location_everywhere').style.display='table-row';
                    if(method_sel=='countries') document.getElementById('location_countries').style.display='table-row';
                    if(method_sel=='postal') document.getElementById('location_postal_code').style.display='table-row';

                    return false;
                    });

                });
            </script>
        </div>
        <?php
    }


    function tt_render_repair_page() {
        global $wpdb, $woocommerce;
        $wpdb->show_errors();

        if(isset($_GET['complete']) && $_GET['complete'] == 'true') :
            $delete_table = $wpdb->query("DROP TABLE IF EXISTS `".$wpdb->prefix."woocommerce_shipping_zones`");
            $sql = "CREATE TABLE IF NOT EXISTS `".$wpdb->prefix."woocommerce_shipping_zones` (
                  `zone_id` int(8) NOT NULL AUTO_INCREMENT,
                  `zone_enabled` int(1) NOT NULL,
                  `zone_title` varchar(200) NOT NULL,
                  `zone_description` longtext NOT NULL,
                  `zone_type` varchar(200) NOT NULL,
                  `zone_country` longtext NOT NULL,
                  `zone_postal` longtext NOT NULL,
                  `zone_order` int(8) NOT NULL,
                  `date_added` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
                  PRIMARY KEY (`zone_id`)
                );";
            $wpdb->query($sql);
            $wpdb->query("INSERT INTO `".$wpdb->prefix."woocommerce_shipping_zones` (`zone_enabled`,`zone_title`,`zone_type`) VALUES ('1','Everywhere','everywhere')");
            $redirect = add_query_arg( array('tab' => 'shipping_zones', 'repair_complete' => 'true', 'action' => '' ));
            wp_safe_redirect( $redirect );
        endif;
        ?>
        <div class="wrap woocommerce">
            
            <div id="icon-users" class="icon32"><br/></div>
            <h2>Repair Shipping Zones Database</h2>
            <p>If you're having trouble creating zones, or wish to start over press the button below to reconstruct the database. <strong>Please note that this action will DELETE the existing table and all data inside.</strong> The table will then be rebuilt with one zone labeled 'Everywhere'.</p>
            <p>This function should ONLY be used to when a problem occurs that cannot be fixed. This most commonly happens during an upgrade or fresh install. It is recommended that you backup your database before proceeding in case you wish to revert the changes later.</p>
            <form>
            <p style="text-align:center;"><a href="?page=<?php echo $_REQUEST['page']; ?>&tab=shipping_zones&action=repair&complete=true" class="button-primary">Repair / Replace Zone DB Table</a></p>
            </form>
        </div>
    <?php   
    }
}

function enable_zone_link() {
    global $wpdb;

    if ( ! is_admin() ) die;
    if ( ! current_user_can('edit_posts') ) wp_die( __('You do not have sufficient permissions to access this page.') );
    if ( ! check_admin_referer('woocommerce-settings')) wp_die( __('You have taken too long. Please go back and retry.') );

    $zone_id = (isset( $_GET['zone_id'] ) && is_numeric($_GET['zone_id'])) ? (int) $_GET['zone_id'] : 0;
    if (!$zone_id) die;

    $zone_verify = $wpdb->get_var("SELECT zone_id FROM ".$wpdb->prefix."woocommerce_shipping_zones WHERE zone_id='".$_GET['zone_id']."'");
    if($zone_verify != 0) {
        $zone_enabled = $wpdb->get_var("SELECT zone_enabled FROM ".$wpdb->prefix."woocommerce_shipping_zones WHERE zone_id='".$_GET['zone_id']."'");
        //$is_enabled = $zone->zone_enabled;

        if ( $zone_enabled == '1' ) {
            $wpdb->query($wpdb->prepare("UPDATE ".$wpdb->prefix."woocommerce_shipping_zones SET zone_enabled='0' WHERE zone_id='".$_GET['zone_id']."'"));
        } else
            $wpdb->query($wpdb->prepare("UPDATE ".$wpdb->prefix."woocommerce_shipping_zones SET zone_enabled='1' WHERE zone_id='".$_GET['zone_id']."'"));
    }
    wp_safe_redirect( remove_query_arg( array('trashed', 'untrashed', 'deleted', 'ids'), wp_get_referer() ) );
}
add_action('wp_ajax_wtrs-zone-enabled', 'enable_zone_link');
