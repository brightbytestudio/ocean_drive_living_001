<?php
/*
For Plugin: WooCommerce Table Rate Shipping
Description: Creates new settings page for zone shipping.
	Additionally functions are defined to compare data for other functions
*/

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

	/**
	 * Check if WooCommerce is active
	 */
	if ( in_array( 'woocommerce/woocommerce.php', apply_filters( 'active_plugins', get_option( 'active_plugins' ) ) ) ) {

		if (!class_exists('WC_Shipping_Method')) return;

		include(ABSPATH . '/wp-content/plugins/woocommerce-table-rate-shipping/inc/zone-list-table.php');

		function create_new_tab() {
	    	$current_tab = ( empty( $_GET['tab'] ) ) ? 'general' : sanitize_text_field( urldecode( $_GET['tab'] ) );

			echo '<a href="' . admin_url( 'admin.php?page=woocommerce_settings&tab=shipping_zones' ) . '" class="nav-tab ';
			if( $current_tab == 'shipping_zones' ) echo 'nav-tab-active';
			echo '">Shipping Zones</a>';
		}
		add_action('woocommerce_settings_tabs','create_new_tab');

		function be_table_rate_shipping_zones() {
			global $woocommerce;

			if(isset($_GET['action']) && ($_GET['action'] == 'edit' || $_GET['action'] == 'new')) :
				Zone_List_Table::tt_render_edit_page();
			elseif(isset($_GET['action']) && $_GET['action'] == 'repair') :
				$GLOBALS['hide_save_button'] = true;
				Zone_List_Table::tt_render_repair_page();
			else :
				$GLOBALS['hide_save_button'] = true;
				Zone_List_Table::tt_render_list_page();
			endif;
		}
		add_action('woocommerce_settings_tabs_shipping_zones','be_table_rate_shipping_zones');

		function be_save_new_zone() {
			global $wpdb, $woocommerce;
			$wpdb->show_errors();

			if(isset($_POST['action'])) {
				if($_POST['action'] == 'delete') {
					if(count($_POST['zone']) > 0) {
						foreach ($_POST['zone'] as $value) {
			                $zone_verify = $wpdb->get_var("SELECT zone_id FROM ".$wpdb->prefix."woocommerce_shipping_zones WHERE zone_id='".$value."'");
			                if($zone_verify != '')  {
			                    $wpdb->query($wpdb->prepare("DELETE FROM ".$wpdb->prefix."woocommerce_shipping_zones WHERE zone_id='".$value."'"));
			                }
						}
					}
					$redirect = add_query_arg( array('saved' => 'true' ));
				} else {
					$i = 1;
					if(count($_POST['zone_id']) > 0) {
						$zone_order = array();
						foreach ($_POST['zone_id'] as $value) {
			                $zone_verify = $wpdb->get_var("SELECT zone_id FROM ".$wpdb->prefix."woocommerce_shipping_zones WHERE zone_id='".$value."'");
			                if($zone_verify != '')  {
			                    $wpdb->update($wpdb->prefix."woocommerce_shipping_zones",
			                    				array('zone_order' => $i),
			                    				array('zone_id' => $value)
			                    );
			                    $zone_order[$value] = $i;
			                    $i++;
			                }
						}
						// reorder table of rates to reflect new zone order
						$currentRates = array_filter( (array) get_option( 'woocommerce_table_rates' ) );
						foreach ($currentRates as $key => $value) {
							$currentRates[$key]['zone_order'] = $zone_order[$value['zone']];
						}
						$newRates = BE_Table_Rate_Shipping::m_array_sort($currentRates, array('zone_order','class_priority','min','title','cost'));
						update_option('woocommerce_table_rates', $newRates);
					}
					$redirect = add_query_arg( array('saved' => 'true' ));
				}
			} elseif ($_GET['action'] == 'new' || $_GET['action'] == 'edit') {
				$zone_id_posted = (int) $_POST['zone_id'];
				$zone_enabled = ( isset( $_POST['zone_enabled'] ) ) ? '1' : '0';
				$zone_title = sanitize_text_field($_POST['zone_title']);
				$zone_description = sanitize_text_field($_POST['zone_description']);
				$zone_type = sanitize_text_field($_POST['zone_type']);

				if($zone_id_posted == 0) $zone_id = $wpdb->get_var(" SELECT AUTO_INCREMENT FROM information_schema.TABLES WHERE TABLE_SCHEMA=DATABASE() AND TABLE_NAME='".$wpdb->prefix."woocommerce_shipping_zones'; ");
				if($zone_id_posted == 0) $zone_order_max = $wpdb->get_var("SELECT MAX(zone_order) FROM ".$wpdb->prefix."woocommerce_shipping_zones");

				if($zone_type == 'countries') {
					$zone_country = ( isset( $_POST[ 'location_countries' ] ) ) ? array_map( 'woocommerce_clean', (array) $_POST[ 'location_countries' ] ) : array();
					$zone_country = implode(',', $zone_country);
					$zone_postal = '';
				} elseif($zone_type == 'postal') {
					$zone_country = sanitize_text_field($_POST['location_country']);
					$zone_postal = sanitize_text_field($_POST['location_codes']);
				} else {
					$zone_country = $zone_postal = "";
				}

				if($zone_id_posted != 0) :
					$wpdb->update(
						$wpdb->prefix.'woocommerce_shipping_zones',
						array(
							'zone_enabled' => $zone_enabled,
							'zone_title' => $zone_title,
							'zone_description' => $zone_description,
							'zone_type' => $zone_type,
							'zone_country' => $zone_country,
							'zone_postal' => $zone_postal
							),
						array('zone_id' => $zone_id_posted)
					);
				else :
					$wpdb->insert(
						$wpdb->prefix.'woocommerce_shipping_zones',
						array(
							'zone_enabled' => $zone_enabled,
							'zone_title' => $zone_title,
							'zone_description' => $zone_description,
							'zone_type' => $zone_type,
							'zone_country' => $zone_country,
							'zone_postal' => $zone_postal,
							'zone_order' => $zone_order_max+1
							)
					);
				endif;

				// Clear any unwanted data
				$woocommerce->clear_product_transients();
				if($zone_id == 0) $zone_id = $zone_id_posted;

				delete_transient( 'woocommerce_cache_excluded_uris' );

				// Redirect back to the settings page
				$redirect = add_query_arg( array('saved' => 'true', 'action' => 'edit', 'zone' => $zone_id ));
			}

			wp_safe_redirect( $redirect );
			exit;
		}
		add_action('woocommerce_update_options_shipping_zones','be_save_new_zone');

		function be_get_zones() {
			global $wpdb;

			$zones = $wpdb->get_results("SELECT zone_id, zone_title FROM ".$wpdb->prefix."woocommerce_shipping_zones ORDER BY zone_order ASC", ARRAY_A);
			return $zones;
		}

		function be_in_zone($zone_id, $country, $state, $zipcode) {
			global $wpdb;

			$zone = $wpdb->get_row("SELECT * FROM ".$wpdb->prefix."woocommerce_shipping_zones WHERE zone_id=".$zone_id." LIMIT 1", ARRAY_A);
			if(count($zone) > 0) :
				if($zone['zone_enabled'] == 0) return false;

				switch ($zone['zone_type']) {
		            case 'everywhere':
		                return true;
		            case 'countries':
                		$countries_abbr = explode(',', $zone['zone_country']);
		    			if(in_array($country, $countries_abbr) || in_array($country.":".$state, $countries_abbr))
		    				return true;
		    			else return false;
		            case 'postal':
		    			if($country == $zone['zone_country'] || $country.":".$state == $zone['zone_country']) {
		    				$zone['zone_postal'] = str_replace( ', ', ',', $zone['zone_postal'] );
		    				if ( $zone['zone_postal'] != '' ) {
								foreach( explode( ',', $zone['zone_postal'] ) as $code ) {
									$code_clean = str_replace('^', '', $code);
			    					if($code_clean == $zipcode) {
										if(!strstr( $code, '^' )) return true; 
											else return false;
			    					} elseif(strstr( $code, '-' )) {
			    						$code_clean = str_replace( ' - ', '-', $code_clean );
			    						list($code_1,$code_2) = explode('-', $code_clean);
			    						$range = range($code_1,$code_2);
			    						if(in_array($zipcode, $range))
											if(!strstr( $code, '^' )) return true; 
												else return false;
			    					} elseif(strstr( $code, '*' )) {
										$code_length = strlen( $code_clean ) - 1;
										if (strtolower(substr($code_clean, 0, -1)) == strtolower(substr($zipcode, 0, $code_length)))
											if(!strstr( $code, '^' )) return true; 
												else return false;
									}
								}
							}
		    			}
					default:
						return false;
				}
			else :
				return false;
			endif;
		}
	}