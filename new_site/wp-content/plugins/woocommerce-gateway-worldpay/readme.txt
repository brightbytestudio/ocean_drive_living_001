How to setup your WorldPay "Installation Administration"

After you have logged into WorldPay click on 'Installations' in the left menu

Now click on the spanner/wrench under 'Integration Setup : TEST' for your 'Select Junior' account

You need to edit the following fields as per the image 'WorldPay-Settings-ScreenShot.jpg', included with this plugin.

'Store-builder used' change to 'Other'
'store-builder: if other - please specify' set as 'WooCommerce'
'Payment Response URL' needs to be EITHER

http://<wpdisplay item="MC_callback">

OR

https://<wpdisplay item="MC_callback">

depending on whether you use http or https for your site

'Payment Response enabled?', 'Enable Recurring Payment Response' and 'Enable the Shopper Response ' all need to be ticked


'Payment Response password' needs to be set

Click 'Save Changes' and then 'OK'


You need to copy the 'Payment Response password' and 'Installation ID' into the 
settings panel of the WorldPay payment gateway in your WooCommerce admin