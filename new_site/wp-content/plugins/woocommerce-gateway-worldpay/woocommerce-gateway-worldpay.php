<?php
/*
Plugin Name: WooCommerce WorldPay Gateway
Plugin URI: http://woothemes.com/woocommerce/
Description: Extends WooCommerce. Provides a WorldPay gateway for WooCommerce. Includes basic support for Subscriptions. http://www.worldpay.com.
Version: 3.0.1
Author: Andrew Benbow
Author URI: http://www.addonenterprises.com
*/

/*  Copyright 2011  Andrew Benbow  (email : andrew@chromeorange.co.uk)

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License, version 2, as
    published by the Free Software Foundation.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
	
	Test environment:
	https://secure-test.worldpay.com/wcc/iadmin (https://secure-test.worldpay.com/wcc/iadmin)

	Production environment:
	https://secure.worldpay.com/wcc/iadmin (https://secure.worldpay.com/wcc/iadmin)
*/

/**
 * Required functions
 */
if ( ! function_exists( 'woothemes_queue_update' ) )
	require_once( 'woo-includes/woo-functions.php' );

/**
 * Plugin updates
 */
woothemes_queue_update( plugin_basename( __FILE__ ), '6bc48c9d12dc0c43add4b099665a80b0', '18646' );

// Init WorldPay Gateway after WooCommerce has loaded
add_action('plugins_loaded', 'init_worldpay_gateway', 0);

function init_worldpay_gateway() {

	if ( ! class_exists( 'WC_Payment_Gateway' ) ) return;

	/*
	 * Gateway class
	 **/
	class WC_Gateway_Worldpay_Form extends WC_Payment_Gateway {

		/*
	 	 * Get all the options and constants
	 	 **/
		public function __construct() {

			$this->id					= 'worldpay';
			$this->method_title 		= __('WorldPay', 'woothemes');
			$this->icon 				= WP_PLUGIN_URL . "/" . plugin_basename( dirname(__FILE__)) . '/images/payment-icons.png';
			$this->has_fields 			= false;
			$this->liveurl 				= 'https://secure.worldpay.com/wcc/purchase';
			$this->testurl 				= 'https://secure-test.worldpay.com/wcc/purchase';

			// Load the form fields
			$this->init_form_fields();

			// Load the settings.
			$this->init_settings();

			// Get setting values
			$this->enabled			= $this->settings['enabled'];
			$this->title 			= $this->settings['title'];
			$this->description  	= $this->settings['description'];
  			$this->status			= $this->settings['status'];
			$this->instId			= $this->settings['instId'];
			$this->callbackPW		= $this->settings['callbackPW'];
			$this->orderDesc		= $this->settings['orderDesc'];
			$this->accid			= $this->settings['accid'];
			$this->authMode			= $this->settings['authMode'];
			$this->fixContact		= $this->settings['fixContact'];
			$this->hideContact		= $this->settings['hideContact'];
			$this->hideCurrency		= $this->settings['hideCurrency'];
			$this->lang				= $this->settings['lang'];
			$this->noLanguageMenu	= $this->settings['noLanguageMenu'];
			$this->worldpaydebug	= $this->settings['worldpaydebug'];
			$this->worldpayurlnotice= $this->settings['worldpayurlnotice'];

			// Hooks
			add_action( 'woocommerce_update_options_payment_gateways', array( $this, 'process_admin_options' ) );
			add_action( 'woocommerce_update_options_payment_gateways_' . $this->id, array( $this, 'process_admin_options' ) );
			add_action( 'woocommerce_api_' . strtolower( get_class( $this ) ), array( $this, 'check_worldpay_response' ) );
			add_action( 'valid-worldpay-request', array( $this, 'successful_request' ) );
			add_action( 'woocommerce_receipt_worldpay', array( $this, 'receipt_page' ) );
			
			/**
			 * Subscriptions
			 * Check if the URL notice has been set to yes, 
			 * If it is not set then no subscriptions for you!
			 */
			if ( $this->worldpayurlnotice == 'yes' ) :
				$this->supports = array(
						'products',
						'subscriptions'
				);
			endif;
			
			/**
			 * We must notify the admin if Subscriptions and WorldPay are active
			 * The current instructions tell customers to set the return to be a dynamic value, 
			 * this won't work with subscriptions, the value at WorldPay has to be fixed.
			 */
			add_action( 'admin_notices', array( $this, 'worldpay_subscription_admin_notice' ) );

		} // END __construct


		/*
    	 * Initialise Gateway Settings Form Fields
    	 **/
    	function init_form_fields() {

    		$this->form_fields = array(
				'enabled' 			=> array(
												'title' 		=> __( 'Enable/Disable', 'woothemes' ),
												'label' 		=> __( 'Enable WorldPay', 'woothemes' ),
												'type' 			=> 'checkbox',
												'description' 	=> '',
												'default' 		=> 'no'
												),

				'title' 			=> array(
												'title' 		=> __( 'Title', 'woothemes' ),
												'type' 			=> 'text',
												'description' 	=> __( 'This controls the title which the user sees during checkout.', 'woothemes' ),
												'default' 		=> __( 'Credit Card via WorldPay', 'woothemes' )
												),

				'description' 		=> array(
												'title' 		=> __( 'Description', 'woothemes' ),
												'type' 			=> 'textarea',
												'description' 	=> __( 'This controls the description which the user sees during checkout.', 'woothemes' ),
												'default' 		=> 'Pay via Credit / Debit Card with WorldPay secure card processing.'
												),

				'status' 			=> array(
												'title' 		=> __( 'Status', 'woothemes' ),
												'type' 			=> 'select',
												'options' 		=> array('live'=>'Live','testing'=>'Testing'),
												'description' 	=> __( 'Set WorldPay Live/Testing Status.', 'woothemes' ),
												'default' 		=> 'testing'
												),

				'instId' 			=> array(
												'title' 		=> __( 'Installation ID', 'woothemes' ),
												'type' 			=> 'text',
												'description' 	=> __( 'This should have been supplied by WorldPay when you created your account.', 'woothemes' ),
												'default' 		=> ''
												),

				'callbackPW' 		=> array(
												'title' 		=> __( 'Callback Password', 'woothemes' ),
												'type' 			=> 'text',
												'description' 	=> __( 'You MUST set this value here and in your WorldPay Installation.', 'woothemes' ),
												'default' 		=> ''
												),

				'orderDesc' 		=> array(
												'title' 		=> __( 'Order Decription', 'woothemes' ),
												'type' 			=> 'text',
												'description' 	=> __( 'This is what appears on the payment screen when the customer lands at WorldPay and is also shown on statements and emails between your store and the shopper.', 'woothemes' ),
												'default' 		=> 'Order with ' .  get_bloginfo('name')
												),

				'accid' 			=> array(
												'title' 		=> __( 'Payment Account ID', 'woothemes' ),
												'type' 			=> 'text',
												'description' 	=> __( 'This specifies which account will receive the funds. Only add account details here if you are not using the default account to receive money, most people will leave this blank.', 'woothemes' ),
												'default' 		=> ''
												),

				'authMode' 			=> array(
												'title' 		=> __( 'Authorisation Mode', 'woothemes' ),
												'type' 			=> 'select',
												'options' 		=> array('A'=>'Full Auth','E'=>'Pre Auth'),
												'description' 	=> __( 'Enable Full Auth or Pre Auth, only change this if you know what you are doing - Pre Auth preauthorises the card but DOES NOT take the funds!', 'woothemes' ),
												'default' 		=> 'A'
												),

				'fixContact' 			=> array(
												'title' 		=> __( 'Fix Customer Info', 'woothemes' ),
												'type' 			=> 'select',
												'options' 		=> array('yes'=>'Yes','no'=>'No'),
												'description' 	=> __( 'If this is set to yes then the customer will not be able to change the information they entered on your site when they get to WorldPay', 'woothemes' ),
												'default' 		=> 'yes'
												),

				'hideContact' 			=> array(
												'title' 		=> __( 'Hide Customer Info', 'woothemes' ),
												'type' 			=> 'select',
												'options' 		=> array('yes'=>'Yes','no'=>'No'),
												'description' 	=> __( 'If this is set to yes then the customer will not be able to see the information they entered on your site when they get to WorldPay', 'woothemes' ),
												'default' 		=> 'yes'
												),

				'hideCurrency' 			=> array(
												'title' 		=> __( 'Hide Currency', 'woothemes' ),
												'type' 			=> 'select',
												'options' 		=> array('yes'=>'Yes','no'=>'No'),
												'description' 	=> __( 'If this is set to no then the customer will be able to change the currency at WorldPay. Exchange rates are set by WorldPay.', 'woothemes' ),
												'default' 		=> 'yes'
												),

				'lang' 					=> array(
												'title' 		=> __( 'Language', 'woothemes' ),
												'type' 			=> 'select',
												'options' 		=> array('yes'=>'Yes','no'=>'No'),
												'description' 	=> __( 'Set a default language shown at WorldPay. If you set the \'Remove Language Menu\' option to NO then this setting determines the Worldpay language.', 'woothemes' ),
												'default' 		=> 'yes'
												),

				'noLanguageMenu' 		=> array(
												'title' 		=> __( 'Remove Language Menu', 'woothemes' ),
												'type' 			=> 'select',
												'options' 		=> array('yes'=>'Yes','no'=>'No'),
												'description' 	=> __( 'This suppresses the display of the language menu at WorldPay', 'woothemes' ),
												'default' 		=> 'yes'
												),

				'worldpaydebug' 		=> array(
												'title' 		=> __( 'Enable Debugging Email', 'woothemes' ),
												'type' 			=> 'select',
												'options' 		=> array('yes'=>'Yes','no'=>'No'),
												'description' 	=> __( 'Send the site admin a debugging email with the form contents', 'woothemes' ),
												'default' 		=> 'yes'
												),

				'worldpayurlnotice' 	=> array(
												'title' 		=> __( 'WorldPay URL Notice', 'woothemes' ),
												'type' 			=> 'select',
												'options' 		=> array('yes'=>'Yes','no'=>'No'),
												'description' 	=> __( 'In previous versions of the WorldPay gateway the instructions were to set the return url to a dynamic value, with subscriptions this is no longer possible. If the subscriptions plugin is active then this MUST be set to yes and the Payment Response URL must be set to ' .get_bloginfo('url'). '/wp-content/plugins/woocommerce-gateway-worldpay/wpcallback.php in your WorldPay Installation Administration. Once you have made the changes you should place a test transaction to confirm everything is working.', 'woothemes' ),
												'default' 		=> 'no'
												)
			);

    	} // END init_form_fields


		/*
 		 * Admin Panel Options
		 * - Options for bits like 'title' and availability on a country-by-country basis
		 **/
		public function admin_options() {
			?>
	    	<h3><?php _e('WorldPay', 'woothemes'); ?></h3>
			<p><?php _e('The WorldPay gateway works by sending the user to <a href="http://www.worldpay.com">WorldPay</a> to enter their payment information.', 'woothemes'); ?></p>
			<table class="form-table">
			<?php
				// Generate the HTML for the settings form.
				$this->generate_settings_html();
			?>
			</table><!--/.form-table-->
			<?php
		} // END admin_options


		/*
 		 * There are no payment fields for WorldPay, but we want to show the description if set.
 		 **/
		function payment_fields() {

			if ($this->description) echo wpautop(wptexturize($this->description));

		} // END payment_fields


		/*
		 * Generate the form button
		 **/
		public function generate_worldpay_form( $order_id ) {
			global $woocommerce;

			$order = new WC_Order( $order_id );
			
			if ( class_exists( 'WC_Subscriptions' ) && WC_Subscriptions_Order::order_contains_subscription( $order_id ) ) :
				
				switch ( strtolower(WC_Subscriptions_Order::get_subscription_period( $order )) ) {
				
					case 'day' :
						$subscription_period = '1';
						break;
					
					case 'week' :
						$subscription_period = '2';
						break;
					
					case 'month' :
						$subscription_period = '3';
						break;
				
					case 'year' :
						$subscription_period = '4';
						break;
				
				}
				
				switch ( strtolower(WC_Subscriptions_Order::get_subscription_trial_period( $order )) ) {
				
					case 'day' :
						$trial_period = '1';
						break;
					
					case 'week' :
						$trial_period = '2';
						break;
					
					case 'month' :
						$trial_period = '3';
						break;
				
					case 'year' :
						$trial_period = '4';
						break;
				
				}
				
			endif;

			if ( $this->status == 'testing' ):
				$worldpayform_adr = $this->testurl;
				$testMode		  = '<input type="hidden" name="testMode" value="100">' . "\r\n";
			else :
				$worldpayform_adr = $this->liveurl;
				$testMode		  = '';
			endif;

			$callbackurl   = '';
			$callbackurl   = WP_PLUGIN_URL . "/" . plugin_basename( dirname(__FILE__)) . '/wpcallback.php';
			$httpreplace   = array("http://","https://");
			$callbackurl   = str_replace($httpreplace,"",$callbackurl);
			$successurl    = str_replace( 'https:', 'http:', add_query_arg( 'wc-api', 'WC_Gateway_Worldpay_Form', home_url( '/' ) ) );
			$failureurl    = $order->get_cancel_order_url();

			$worldpayform  = $testMode;
			$worldpayform .= '<input type="hidden" name="instId" value="'.$this->instId.'">' . "\r\n";
			$worldpayform .= '<input type="hidden" name="cartId" value="' .$order->order_key . '-' . $order->id . '-' . time(). '">' . "\r\n";
			/**
			 * Modify the order amount for subscriptions
			 *
			 * If there is a subscription we get the amount from WC_Subscriptions_Order::get_total_initial_payment( $order )
			 * otherwise it's just the oder total
			 *
			 * WC_Subscriptions_Order::get_total_initial_payment( $order ) works out if there is a payment due today, 
			 * if not this value will be 0 and no money will be taken today
			 */
			if( class_exists( 'WC_Subscriptions' ) && WC_Subscriptions_Order::order_contains_subscription( $order_id ) ) :
				$worldpayform .= '<input type="hidden" name="amount" value="'.WC_Subscriptions_Order::get_total_initial_payment( $order ).'">' . "\r\n";
			else :
				$worldpayform .= '<input type="hidden" name="amount" value="'.$order->order_total.'">' . "\r\n";
			endif;
			$worldpayform .= '<input type="hidden" name="currency" value="'.get_option('woocommerce_currency').'">' . "\r\n";
			$worldpayform .= '<input type="hidden" name="desc" value="'.$this->orderDesc.'">' . "\r\n";
			$worldpayform .= '<input type="hidden" name="name" value="' .$order->billing_first_name. ' ' .$order->billing_last_name. '">' . "\r\n";
			$worldpayform .= '<input type="hidden" name="address1" value="' .$order->billing_address_1. '">' . "\r\n";
			$worldpayform .= '<input type="hidden" name="address2" value="' .$order->billing_address_2. '">' . "\r\n";
			$worldpayform .= '<input type="hidden" name="address3" value="">' . "\r\n";
			$worldpayform .= '<input type="hidden" name="town" value="' .$order->billing_city. '">' . "\r\n";
			$worldpayform .= '<input type="hidden" name="region" value="' .$order->billing_state. '">' . "\r\n";
			$worldpayform .= '<input type="hidden" name="postcode" value="' .$order->billing_postcode. '">' . "\r\n";
			$worldpayform .= '<input type="hidden" name="country" value="' .$order->billing_country. '">' . "\r\n";
			$worldpayform .= '<input type="hidden" name="tel" value="' .$order->billing_phone. '">' . "\r\n";
			$worldpayform .= '<input type="hidden" name="email" value="' .$order->billing_email. '">' . "\r\n";

			if ( $this->fixContact == 'yes' ) :
				$worldpayform .= '<input type="hidden" name="fixContact" value="">' . "\r\n";
			endif;

			if ( $this->hideContact == 'yes' ) :
				$worldpayform .= '<input type="hidden" name="hideContact">' . "\r\n";
			endif;

			if ( $this->accid != '' || isset($this->accid) ) :
				$worldpayform .= '<input type="hidden" name="accid" value="'.$this->accid.'">' . "\r\n";
			endif;

			if ( $this->authMode == 'A' || $this->authMode == 'E' ) :
				$worldpayform .= '<input type="hidden" name="authMode" value="'.$this->authMode.'">' . "\r\n";
			endif;

			if ( $this->hideCurrency == 'yes' ) :
				$worldpayform .= '<input type="hidden" name="hideCurrency">' . "\r\n";
			endif;

			if ( $this->lang != '' || isset($this->lang) ) :
				$worldpayform .= '<input type="hidden" name="lang" value="'.$this->lang.'">' . "\r\n";
			endif;

			if ( $this->noLanguageMenu == 'yes' ) :
				$worldpayform .= '<input type="hidden" name="noLanguageMenu">' . "\r\n";
			endif;
			/**
			 * Adding a lot more form values for Subscriptions
			 */
			if ( class_exists( 'WC_Subscriptions' ) && WC_Subscriptions_Order::order_contains_subscription( $order_id ) ) :
				$worldpayform .= '<!-- Futurepay parameters start here -->' . "\r\n";
				$worldpayform .= '<INPUT TYPE="HIDDEN" NAME=futurePayType 			VALUE="regular">' 				. "\r\n";
				
				/**
				 * If the subscription period is less than 2 weeks the option must be 0 which means no modifications
				 */
				if ( $subscription_period == '1' || ( $subscription_period == '2' && WC_Subscriptions_Order::get_subscription_interval( $order ) <= '2' ) ) :
					$worldpayform .= '<INPUT TYPE="HIDDEN" NAME=option 					VALUE=0>' . "\r\n";
				else :
					$worldpayform .= '<INPUT TYPE="HIDDEN" NAME=option 					VALUE=1>' . "\r\n";
				endif;
				
				/**
				 * Set start date if there is a trial period or use subscription period settings
				 * 
				 * Use strtotime because subscriptions passes an INT for the length and a word for period
				 * doing it any other way means a messy calculation
				 */
				if ( WC_Subscriptions_Order::get_subscription_trial_length( $order ) >= 1 ) :
					$worldpayform .= '<INPUT TYPE="HIDDEN" NAME=startDate 	  	  	VALUE='.date("Y-m-d",strtotime("+" . WC_Subscriptions_Order::get_subscription_trial_length( $order ) ." " .WC_Subscriptions_Order::get_subscription_trial_period( $order ))) .'>' . "\r\n";
				else :
					$worldpayform .= '<INPUT TYPE="HIDDEN" NAME=startDelayMult 		VALUE='. WC_Subscriptions_Order::get_subscription_interval( $order ) .'>' . "\r\n";
					$worldpayform .= '<INPUT TYPE="HIDDEN" NAME=startDelayUnit 		VALUE='.$subscription_period.'>' . "\r\n";
				endif;
				
				$worldpayform .= '<INPUT TYPE="HIDDEN" NAME=noOfPayments 			VALUE='.WC_Subscriptions_Order::get_subscription_length( $order ).'>' . "\r\n";
				$worldpayform .= '<INPUT TYPE="HIDDEN" NAME=intervalMult 			VALUE='.WC_Subscriptions_Order::get_subscription_interval( $order ).'>' . "\r\n";
				$worldpayform .= '<INPUT TYPE="HIDDEN" NAME=intervalUnit 			VALUE='.$subscription_period.'>' . "\r\n";
				//if ( $subscription_period == '1' || ( $subscription_period == '2' && WC_Subscriptions_Order::get_subscription_interval( $order ) <= '2' ) ) :
					$worldpayform .= '<INPUT TYPE="HIDDEN" NAME=normalAmount 		VALUE='.WC_Subscriptions_Order::get_recurring_total( $order ).'>' . "\r\n";
				//endif;
				
				if ( $this->worldpaydebug == 'yes' ) :
					// Attach all the subscription variables and the names from WooCommerce Subscriptions so we can work out what the hell is going on here!
					$worldpayform .= '<INPUT TYPE="HIDDEN" NAME=initialPayment 	 	  VALUE='.WC_Subscriptions_Order::get_total_initial_payment( $order ).'>' . "\r\n";
					$worldpayform .= '<INPUT TYPE="HIDDEN" NAME=recurring_total 	  VALUE='.WC_Subscriptions_Order::get_recurring_total( $order ).'>' . "\r\n";
					$worldpayform .= '<INPUT TYPE="HIDDEN" NAME=sign_up_fee 		  VALUE='.WC_Subscriptions_Order::get_sign_up_fee( $order ).'>' . "\r\n";
					$worldpayform .= '<INPUT TYPE="HIDDEN" NAME=subscription_period   VALUE='.WC_Subscriptions_Order::get_subscription_period( $order ).'>' . "\r\n";
					$worldpayform .= '<INPUT TYPE="HIDDEN" NAME=subscription_length   VALUE='.WC_Subscriptions_Order::get_subscription_length( $order ).'>' . "\r\n";
					$worldpayform .= '<INPUT TYPE="HIDDEN" NAME=subscription_interval VALUE='.WC_Subscriptions_Order::get_subscription_interval( $order ).'>' . "\r\n";
					$worldpayform .= '<INPUT TYPE="HIDDEN" NAME=trial_length 	  	  VALUE='.WC_Subscriptions_Order::get_subscription_trial_length( $order ).'>' . "\r\n";
					$worldpayform .= '<INPUT TYPE="HIDDEN" NAME=trial_period 	  	  VALUE='.WC_Subscriptions_Order::get_subscription_trial_period( $order ).'>' . "\r\n";
					$worldpayform .= '<INPUT TYPE="HIDDEN" NAME=strtotime 	  	  	  VALUE='.date("Y-m-d",strtotime("+" . WC_Subscriptions_Order::get_subscription_trial_length( $order ) ." " .WC_Subscriptions_Order::get_subscription_trial_period( $order ))) .'>' . "\r\n";
				endif;

				$worldpayform .= '<!-- Futurepay parameters end here -->' . "\r\n";
			endif; 

			$worldpayform .= '<input type="hidden" name="MC_callback" 			value="'.$callbackurl.'">' . "\r\n";
			$worldpayform .= '<input type="hidden" name="CM_SuccessURL" 		value="'.$successurl.'">' . "\r\n";
			$worldpayform .= '<input type="hidden" name="CM_FailureURL" 		value="'.$failureurl.'">' . "\r\n";
			$worldpayform .= '<input type="hidden" name="CM_order" 				value="'.$order->id.'">' . "\r\n";
			$worldpayform .= '<input type="hidden" name="MC_transactionNumber" 	value="1">' . "\r\n";
			
			if ( $this->worldpaydebug == 'yes' ) :
				wp_mail( get_bloginfo('admin_email'),'Worldpay Debug Message - Form Contents',$worldpayform );
			endif;		

			/*
			 * This is the form.
			 **/
			return  '<form action="'.$worldpayform_adr.'" method="post" id="worldpay_payment_form">
					' . $worldpayform . '
					<input type="submit" class="button-alt" id="submit_worldpay_payment_form" value="'.__('Pay via WorldPay', 'woothemes').'" /> <a class="button cancel" href="'.$order->get_cancel_order_url().'">'.__('Cancel order &amp; restore cart', 'woothemes').'</a>
					<script type="text/javascript">
						jQuery(function(){
							jQuery("body").block({

								message: "<img src=\"'.$woocommerce->plugin_url().'/assets/images/ajax-loader.gif\" alt=\"Redirecting...\" />'.__('Thank you for your order. We are now redirecting you to WorldPay to make payment.', 'woothemes').'",
								overlayCSS:
								{
									background: "#fff",
									opacity: 0.6
								},
								css:
								{
						        	padding:        20,
							        textAlign:      "center",
							        color:          "#555",
							        border:         "3px solid #aaa",
							        backgroundColor:"#fff",
							        cursor:         "wait",
							        lineHeight:		"32px"
						    	}

								});
							jQuery("#submit_worldpay_payment_form").click();
						});
					</script>
				</form>';


		} // END generate_form


		/**
		 * Process the payment and return the result
		 */
		function process_payment( $order_id ) {

			$order = new WC_Order( $order_id );
			return array(
				'result' 	=> 'success',
				'redirect'	=> add_query_arg('order', $order->id, add_query_arg('key', $order->order_key, get_permalink(get_option('woocommerce_pay_page_id'))))
			);

		} // END process_payment

		/**
		 * receipt_page
		 */
		function receipt_page( $order ) {

			echo '<p>'.__('Thank you for your order, please click the button below to pay with WorldPay.', 'woothemes').'</p>';
			echo $this->generate_worldpay_form( $order );

		} // END receipt_page

		/**
		 * Check for WorldPay Response
 		 */
		function check_worldpay_response() {
			global $woocommerce;

			if ( isset($_REQUEST["crypt"]) ) :	

				$crypt 					= $_REQUEST["crypt"];
				$worldpay_return_data   = $this->worldpaysimpleXor($this->base64Decode($_REQUEST["crypt"]), $this->callbackPW);
				$worldpay_return_values = $this->getTokens( $worldpay_return_data );
				
				if ( isset($worldpay_return_values['transId']) ) :
        			do_action("valid-worldpay-request", $worldpay_return_values);
				endif;

   			endif;

   			wp_safe_redirect( get_permalink( get_option( 'woocommerce_thanks_page_id' ) ) );
			exit;
		} // END check_worldpay_response


		/**
		 * Successful Payment!
 		 */
		function successful_request( $worldpay_return_values ) {
			global $wpdb;
			
			$order 	 = new WC_Order( (int) $worldpay_return_values['order'] );
			
			/**
			 * Make sure the order notes contain the FuturePayID
			 * and add it as post_meta so we can find it easily when WorldPay sends 
			 * updates about payments / cancellations etc
			 */
			$orderNotes  = ''; 
			if ( class_exists( 'WC_Subscriptions' ) && WC_Subscriptions_Order::order_contains_subscription( $order->id ) ) :
				$orderNotes .=	'<br /><!-- FUTURE PAY-->';
				$orderNotes .=	'<br />FuturePayID : ' 	. $worldpay_return_values['futurePayId'];
				$orderNotes .=	'<br /><!-- FUTURE PAY-->';
				update_post_meta( $order->id, '_futurepayid', $worldpay_return_values['futurePayId'] );
			endif;

			$orderNotes .=	'<br />transId : ' 			. $worldpay_return_values['transId'];
			$orderNotes .=	'<br />transStatus : ' 		. $worldpay_return_values['transStatus'];
			$orderNotes .=	'<br />transTime : '		. $worldpay_return_values['transTime'];
			$orderNotes .=	'<br />authAmount : ' 		. $worldpay_return_values['authAmount'];
			$orderNotes .=	'<br />authCurrency : ' 	. $worldpay_return_values['authCurrency'];
			$orderNotes .=	'<br />authAmountString : ' . $worldpay_return_values['authAmountString'];
			$orderNotes .=	'<br />rawAuthMessage : ' 	. $worldpay_return_values['rawAuthMessage'];
			$orderNotes .=	'<br />rawAuthCode : ' 		. $worldpay_return_values['rawAuthCode'];
			$orderNotes .=	'<br />cardType : ' 		. $worldpay_return_values['cardType'];
			$orderNotes .=	'<br />countryMatch : ' 	. $worldpay_return_values['countryMatch'];
			$orderNotes .=	'<br />AVS : ' 				. $worldpay_return_values['AVS'];
			
			$order->add_order_note( __('WorldPay payment completed.' . $orderNotes, 'woothemes') );
			
			if ( $this->worldpaydebug == 'yes' ) :
				$debugcontent = $orderNotes . '<br />transactionNumber' . 
								$worldpay_return_values['MC_transactionNumber'] . '<br />' .
								'futurePayStatusChange' . $worldpay_return_values['futurePayStatusChange'];
								
				wp_mail( get_bloginfo('admin_email'),'Worldpay Debug Message - Returned Values',$debugcontent );
			endif;
			
			/**
			 * Check MC_transactionNumber
			 * if this is 1 then this is either the first transaction for a subscription
			 * or the only transction for a none subscription order
			 */
			if ( $worldpay_return_values['MC_transactionNumber'] == '1' ) :
			
				// Normal transaction at the front end
	        	$order->payment_complete();
				wp_safe_redirect( add_query_arg('key', $order->order_key, add_query_arg('order', $order->id, get_permalink(get_option('woocommerce_thanks_page_id')))) );
				exit;
				
			endif;

		} // END successful_request

		function worldpay_subscription_admin_notice() {
			
			if ( $this->worldpayurlnotice != 'yes' && class_exists( 'WC_Subscriptions' ) ) :
?>			
			<div id="message" class="updated warning">
			<p>You MUST confirm that you have set the WorldPay Payment Response URL to <?php echo get_bloginfo('url'); ?> /wp-content/plugins/woocommerce-gateway-worldpay/wpcallback.php in your WorldPay Installation Administration.</p> 
            <p>Once the WorldPay Payment Response URL is set correctly then you should edit the WorldPay settings in WooCommerce and change the WorldPay URL Notice to yes</p>
            <p>Once you have made the changes you should place a test transaction to confirm everything is working.</p>
			</div>
<?php				
			endif;
			
		}

		function base64Decode($scrambled) 	{
			// Initialise output variable
			$output = "";
	
			// Fix plus to space conversion issue
			$scrambled = str_replace(" ", "+", $scrambled);

			// Do decoding
			$output = base64_decode($scrambled);

			// Return the result
			return $output;
		} // END base64Decode

		/*
		 * A Simple Xor encryption algorithm
		 **/
		function worldpaysimpleXor($text, $key) {
			// Initialise key array
			$key_ascii_array = array();

			// Initialise output variable
			$output = "";

			// Convert $key into array of ASCII values
			for($i = 0; $i < strlen($key); $i++){
				$key_ascii_array[$i] = ord(substr($key, $i, 1));
			}

			// Step through string a character at a time
			for($i = 0; $i < strlen($text); $i++) {
				// Get ASCII code from string, get ASCII code from key (loop through with MOD), XOR the
				// two, get the character from the result
				$output .= chr(ord(substr($text, $i, 1)) ^ ($key_ascii_array[$i % strlen($key)]));
			}

			// Return the result
			return $output;
		} // END simpleXor

		/*
		 * A convenience function that extracts the values from the query string.
		 * Works even if one of the values is a URL containing the & or = signs.
		 **/
		function getTokens($query_string) {
			// List the possible tokens
			$tokens = array(
					'order',
					'transId',
					'transStatus',
					'transTime',
					'authAmount',
					'authCurrency',
					'authAmountString',
					'rawAuthMessage',
					'rawAuthCode',
					'callbackPW',
					'cardType',
					'countryMatch',
					'AVS',
					'MC_transactionNumber',
					'futurePayId',
					'futurePayStatusChange'
				);

			// Initialise arrays
			$output = array();
			$tokens_found = array();

			// Get the next token in the sequence
			for ($i = count($tokens) - 1; $i >= 0; $i--){
				// Find the position in the string
				$start = strpos($query_string, $tokens[$i]);

				// If token is present record its position and name
				if ($start !== false){
					$tokens_found[$i]->start = $start;
					$tokens_found[$i]->token = $tokens[$i];
				}
			}

			// Sort in order of position
			sort($tokens_found);

			// Go through the result array, getting the token values
			for ($i = 0; $i < count($tokens_found); $i++){
			// Get the start point of the value
				$valueStart = $tokens_found[$i]->start + strlen($tokens_found[$i]->token) + 1;

				// Get the length of the value
				if ($i == (count($tokens_found) - 1)) {
					$output[$tokens_found[$i]->token] = substr($query_string, $valueStart);
				} else {
					$valueLength = $tokens_found[$i +1 ]->start - $tokens_found[$i]->start - strlen($tokens_found[$i]->token) - 2;
					$output[$tokens_found[$i]->token] = substr($query_string, $valueStart, $valueLength);
				}
			}

			// Return the output array
			return $output;

		} // END getTokens

	} // END CLASS

	/*
	 * Add the Gateway to WooCommerce
	 **/
	function add_worldpay_gateway($methods) {
		$methods[] = 'WC_Gateway_Worldpay_Form';
		return $methods;
	}
	add_filter('woocommerce_payment_gateways', 'add_worldpay_gateway' );

} // END init_worldpay_gateway