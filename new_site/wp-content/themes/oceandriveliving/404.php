<?php get_header(); ?>
		<div class="full" id="title">
			<div class="inner">
				<h2><?php the_title(); ?></h2>
			</div>
		</div>			
		<div class="full">
			<div class="inner">
				<?php the_content(); ?>
			</div>
		</div>	
		<?php wp_link_pages(array('before' => 'Pages: ', 'next_or_number' => 'number')); ?>
<?php get_footer(); ?>
