<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
    "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" <?php language_attributes(); ?>>
<head profile="http://gmpg.org/xfn/11">	
	<meta http-equiv="Content-Type" content="<?php bloginfo('html_type'); ?>; charset=<?php bloginfo('charset'); ?>" />	
	<meta name="viewport" content="width=320, initial-scale=1.0">
	<?php if (is_search()) { ?>
	   <meta name="robots" content="noindex, nofollow" /> 
	<?php } ?>
	<title>
		   <?php
		      if (function_exists('is_tag') && is_tag()) {
		         single_tag_title("Tag Archive for &quot;"); echo '&quot; - '; }
		      elseif (is_archive()) {
		         wp_title(''); echo ' Archive - '; }
		      elseif (is_search()) {
		         echo 'Search for &quot;'.wp_specialchars($s).'&quot; - '; }
		      elseif (!(is_404()) && (is_single()) || (is_page())) {
		         wp_title(''); echo ' - '; }
		      elseif (is_404()) {
		         echo 'Not Found - '; }
		      if (is_home()) {
		         bloginfo('name'); echo ' - '; bloginfo('description'); }
		      else {
		          bloginfo('name'); }
		      if ($paged>1) {
		         echo ' - page '. $paged; }
		   ?>
	</title>	
	<link rel="shortcut icon" href="<?php bloginfo('template_url'); ?>/images/favicon.ico" type="image/x-icon" />	
	<link rel="stylesheet" href="<?php bloginfo('stylesheet_url'); ?>" type="text/css" />	
	<link rel="pingback" href="<?php bloginfo('pingback_url'); ?>" />
	<?php if ( is_singular() ) wp_enqueue_script( 'comment-reply' ); ?>
	<link rel="stylesheet" type="text/css" href="<?php echo get_bloginfo('template_directory');?>/css/nivo-slider.css">
	<?php wp_head(); ?>	
</head>
<body <?php body_class(); ?>>
<div class="full"  id="top">
	<div class="inner">
	<ul id="social">
		<li>
			<a href="http://www.instagram.com/oceandriveliving" target="_new"><img src="<?php echo get_bloginfo('template_directory');?>/images/instagram.jpg">
			Instagram</a>
		</li>
		<li>
			<a href="https://twitter.com/ODLWinchester" target="_new"><img src="<?php echo get_bloginfo('template_directory');?>/images/twittericon.png">
			Twitter</a>
		</li>
	</ul>
		<?php get_search_form( $echo ); ?>
	</div>
</div>	
<div id="header" class="full" style="background: #fff;">
	<div class="inner" id="logocontainer">
	<div class="logoleft">
		<a href="<?php echo site_url();?>"><img src="<?php echo get_bloginfo('template_directory');?>/images/oceandriveliving.jpg" id="logo"></a>
	</div>
	<div class="logoright">
	<div class="f-l">
	<a class="cart_contents" href="<?php echo site_url();?>/cart">
		<img src="<?php echo get_bloginfo('template_directory');?>/images/odlbag.png" id="basket_icon">
		</a>
		</div>
		<div class="f-l" style="padding-top: 10px; padding-left: 5px;">
<span id="cartinfo"><a class="cart_contents" href="<?php echo site_url();?>/cart">Shopping Bag:</a><br /> <?php global $woocommerce; ?><a class="cart_contents" href="<?php echo $woocommerce->cart->get_cart_url(); ?>" title="<?php _e('View your shopping cart', 'woothemes'); ?>"><?php echo sprintf(_n('%d item', '%d items', $woocommerce->cart->cart_contents_count, 'woothemes'), $woocommerce->cart->cart_contents_count);?> - <?php echo $woocommerce->cart->get_cart_total(); ?></a></span>			
</div>
	</div>
	</div>
</div>
<div class="full globalnav" id="header">
	<div class="inner">
		<ul id="menu-global-menu" class="menu"><li id="menu-item-502" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-home menu-item-502"><a href="<?php echo site_url();?>/">Home</a></li>
		<li id="menu-item-503" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-503"><a href="<?php echo site_url();?>/about-us/">About Us</a></li>
		<li id="menu-item-505" class="menu-item menu-item-type-custom menu-item-object-custom current-menu-ancestor current-menu-parent menu-item-505"><a href="#" style="cursor: default">Products</a>
		<ul class="sub-menu">
			<li><h1>Type</h1></li>
			<li id="menu-item-509" class="menu-item menu-item-type-taxonomy menu-item-object-product_cat menu-item-509"><a href="<?php echo site_url();?>/product-category/type/accessories/">Accessories</a></li>
			<li id="menu-item-510" class="menu-item menu-item-type-taxonomy menu-item-object-product_cat current-menu-item menu-item-510"><a href="<?php echo site_url();?>/product-category/type/furniture/">Furniture</a></li>
			<li id="menu-item-511" class="menu-item menu-item-type-taxonomy menu-item-object-product_cat menu-item-511"><a href="<?php echo site_url();?>/product-category/type/cushions/">Cushions</a></li>
			<li id="menu-item-512" class="menu-item menu-item-type-taxonomy menu-item-object-product_cat menu-item-512"><a href="<?php echo site_url();?>/product-category/type/glassware/">Glassware</a></li>
			<li id="menu-item-513" class="menu-item menu-item-type-taxonomy menu-item-object-product_cat menu-item-513"><a href="<?php echo site_url();?>/product-category/type/lighting/">Lighting</a></li>
		</ul>
		<ul class="sub-menu2">
		<li><h1>Brand</h1></li>
			<li id="menu-item-509" class="menu-item menu-item-type-taxonomy menu-item-object-product_cat menu-item-509"><a href="<?php echo site_url();?>/product-category/brands/by-nord/">By Nord</a></li>
			<li id="menu-item-510" class="menu-item menu-item-type-taxonomy menu-item-object-product_cat current-menu-item menu-item-510"><a href="<?php echo site_url();?>/product-category/brands/culinary-concepts-living/">Culinary Concepts Living</a></li>
			<li id="menu-item-511" class="menu-item menu-item-type-taxonomy menu-item-object-product_cat menu-item-511"><a href="<?php echo site_url();?>/product-category/brands/fenella-smith//">Fenella Smith</a></li>
			<li id="menu-item-512" class="menu-item menu-item-type-taxonomy menu-item-object-product_cat menu-item-512"><a href="<?php echo site_url();?>/product-category/brands/finsdottir/">Finnsdottir</a></li>
			<li id="menu-item-514"><a href="<?php echo site_url();?>/product-category/brands/lily-matthews/">Lily Matthews</a></li>
			<li id="menu-item-513" class="menu-item menu-item-type-taxonomy menu-item-object-product_cat menu-item-513"><a href="<?php echo site_url();?>/product-category/brands/scintilla/">Scintilla</a></li>
			<li id="menu-item-513" class="menu-item menu-item-type-taxonomy menu-item-object-product_cat menu-item-513"><a href="<?php echo site_url();?>/product-category/brands/skagerak/">Skagerak</a></li>			
		</ul>		
		</li>
		<li id="menu-item-507" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-507"><a href="<?php echo site_url();?>/delivery-information/">Delivery</a></li>
		<li id="menu-item-508" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-508"><a href="<?php echo site_url();?>/contact-us/">Contact Us</a></li>
		</ul>
		<!--<?php wp_nav_menu( array( 'sort_column' => 'menu_order', 'theme_location' => 'global-menu' ) ); ?>-->
	</div>
</div>
