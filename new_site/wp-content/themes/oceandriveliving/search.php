<?php get_header(); ?>
		<div class="full">
		<div class="inner innermain" style="margin-top: 0">
	<?php if (have_posts()) : ?>



		<?php while (have_posts()) : the_post(); ?>

			<div <?php post_class() ?> id="post-<?php the_ID(); ?>">

				<a href="<?php the_permalink();?>"><h2><?php the_title(); ?></h2></a>


				<div class="entry searchresults">

					<?php the_post_thumbnail('medium'); ?>
					<?php the_excerpt(); ?>
					<a href="<?php the_permalink();?>" class="button">Learn More</a>

				</div>

			</div>

		<?php endwhile; ?>


	<?php else : ?>

		<h2>No products found.</h2>

	<?php endif; ?>
</div></div>
<style type="text/css">

.searchresults {
	width: 100%;
	float: left;
	margin-bottom: 40px;
}
	.searchresults img {
		width: 150px;
		height: auto;
		float: left;
		margin: 0 40px 0 0;
		border: 1px solid #eee;
	}

</style>
<?php get_footer(); ?>
